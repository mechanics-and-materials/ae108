// © 2020 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of ae108.
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/meshing/generate_triangle_mesh.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using testing::DoubleEq;
using testing::Eq;
using testing::IsEmpty;

namespace ae108::meshing {
namespace {

TEST(generate_triangle_mesh_Test, returns_correct_mesh_for_zero_granularity) {
  const auto mesh = generate_triangle_mesh({{1., 1.}}, {{0, 0}});

  EXPECT_THAT(mesh.positions, IsEmpty());
  EXPECT_THAT(mesh.connectivity, IsEmpty());
}

TEST(generate_triangle_mesh_Test,
     returns_correct_connectivity_for_granularity_1_1) {
  const auto mesh = generate_triangle_mesh({{1., 1.}}, {{1, 1}});

  ASSERT_THAT(mesh.connectivity.size(), Eq(2));
  EXPECT_THAT(mesh.connectivity.at(0).at(0), Eq(0));
  EXPECT_THAT(mesh.connectivity.at(0).at(1), Eq(2));
  EXPECT_THAT(mesh.connectivity.at(0).at(2), Eq(1));
  EXPECT_THAT(mesh.connectivity.at(1).at(0), Eq(2));
  EXPECT_THAT(mesh.connectivity.at(1).at(1), Eq(3));
  EXPECT_THAT(mesh.connectivity.at(1).at(2), Eq(1));
}

TEST(generate_triangle_mesh_Test,
     returns_correct_positions_for_granularity_1_1) {
  const auto mesh = generate_triangle_mesh({{1., 1.}}, {{1, 1}});

  ASSERT_THAT(mesh.positions.size(), Eq(4));
  EXPECT_THAT(mesh.positions[0].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[0].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(1), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[2].at(0), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[2].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[3].at(0), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[3].at(1), DoubleEq(1.));
}

TEST(generate_triangle_mesh_Test, returns_correct_positions_for_x_size_2) {
  const auto mesh = generate_triangle_mesh({{2., 1.}}, {{1, 1}});

  ASSERT_THAT(mesh.positions.size(), Eq(4));
  EXPECT_THAT(mesh.positions[0].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[0].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(1), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[2].at(0), DoubleEq(2.));
  EXPECT_THAT(mesh.positions[2].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[3].at(0), DoubleEq(2.));
  EXPECT_THAT(mesh.positions[3].at(1), DoubleEq(1.));
}

TEST(generate_triangle_mesh_Test, returns_correct_positions_for_y_size_2) {
  const auto mesh = generate_triangle_mesh({{1., 2.}}, {{1, 1}});

  ASSERT_THAT(mesh.positions.size(), Eq(4));
  EXPECT_THAT(mesh.positions[0].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[0].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(1), DoubleEq(2.));
  EXPECT_THAT(mesh.positions[2].at(0), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[2].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[3].at(0), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[3].at(1), DoubleEq(2.));
}

TEST(generate_triangle_mesh_Test,
     returns_correct_connectivity_for_granularity_2_1) {
  const auto mesh = generate_triangle_mesh({{2., 1.}}, {{2, 1}});

  ASSERT_THAT(mesh.connectivity.size(), Eq(4));
  EXPECT_THAT(mesh.connectivity.at(0).at(0), Eq(0));
  EXPECT_THAT(mesh.connectivity.at(0).at(1), Eq(2));
  EXPECT_THAT(mesh.connectivity.at(0).at(2), Eq(1));
  EXPECT_THAT(mesh.connectivity.at(1).at(0), Eq(2));
  EXPECT_THAT(mesh.connectivity.at(1).at(1), Eq(3));
  EXPECT_THAT(mesh.connectivity.at(1).at(2), Eq(1));
  EXPECT_THAT(mesh.connectivity.at(2).at(0), Eq(2));
  EXPECT_THAT(mesh.connectivity.at(2).at(1), Eq(4));
  EXPECT_THAT(mesh.connectivity.at(2).at(2), Eq(3));
  EXPECT_THAT(mesh.connectivity.at(3).at(0), Eq(4));
  EXPECT_THAT(mesh.connectivity.at(3).at(1), Eq(5));
  EXPECT_THAT(mesh.connectivity.at(3).at(2), Eq(3));
}

TEST(generate_triangle_mesh_Test,
     returns_correct_positions_for_granularity_2_1) {
  const auto mesh = generate_triangle_mesh({{2., 1.}}, {{2, 1}});

  ASSERT_THAT(mesh.positions.size(), Eq(6));
  EXPECT_THAT(mesh.positions[0].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[0].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(1), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[2].at(0), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[2].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[3].at(0), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[3].at(1), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[4].at(0), DoubleEq(2.));
  EXPECT_THAT(mesh.positions[4].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[5].at(0), DoubleEq(2.));
  EXPECT_THAT(mesh.positions[5].at(1), DoubleEq(1.));
}

TEST(generate_triangle_mesh_Test,
     returns_correct_connectivity_for_granularity_1_2) {
  const auto mesh = generate_triangle_mesh({{1., 2.}}, {{1, 2}});

  ASSERT_THAT(mesh.connectivity.size(), Eq(4));
  EXPECT_THAT(mesh.connectivity.at(0).at(0), Eq(0));
  EXPECT_THAT(mesh.connectivity.at(0).at(1), Eq(3));
  EXPECT_THAT(mesh.connectivity.at(0).at(2), Eq(1));
  EXPECT_THAT(mesh.connectivity.at(1).at(0), Eq(3));
  EXPECT_THAT(mesh.connectivity.at(1).at(1), Eq(4));
  EXPECT_THAT(mesh.connectivity.at(1).at(2), Eq(1));
  EXPECT_THAT(mesh.connectivity.at(2).at(0), Eq(1));
  EXPECT_THAT(mesh.connectivity.at(2).at(1), Eq(4));
  EXPECT_THAT(mesh.connectivity.at(2).at(2), Eq(2));
  EXPECT_THAT(mesh.connectivity.at(3).at(0), Eq(4));
  EXPECT_THAT(mesh.connectivity.at(3).at(1), Eq(5));
  EXPECT_THAT(mesh.connectivity.at(3).at(2), Eq(2));
}

TEST(generate_triangle_mesh_Test,
     returns_correct_positions_for_granularity_1_2) {
  const auto mesh = generate_triangle_mesh({{1., 2.}}, {{1, 2}});

  ASSERT_THAT(mesh.positions.size(), Eq(6));
  EXPECT_THAT(mesh.positions[0].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[0].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[1].at(1), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[2].at(0), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[2].at(1), DoubleEq(2.));
  EXPECT_THAT(mesh.positions[3].at(0), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[3].at(1), DoubleEq(0.));
  EXPECT_THAT(mesh.positions[4].at(0), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[4].at(1), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[5].at(0), DoubleEq(1.));
  EXPECT_THAT(mesh.positions[5].at(1), DoubleEq(2.));
}

} // namespace
} // namespace ae108::meshing
