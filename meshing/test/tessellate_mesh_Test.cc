// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/meshing/tessellate_mesh.h"
#include <array>
#include <gmock/gmock.h>

namespace ae108::meshing {

using Mesh = MeshGeometry<std::array<double, 3>, std::array<std::size_t, 2>>;

Mesh unit_box() {
  return {{{0., 0., 0.},
           {1., 0., 0.},
           {0., 1., 0.},
           {1., 1., 0.},
           {0., 0., 1.},
           {1., 0., 1.},
           {0., 1., 1.},
           {1., 1., 1.}},
          {{0, 1},
           {0, 2},
           {1, 3},
           {2, 3},
           {0, 4},
           {1, 5},
           {2, 6},
           {3, 7},
           {4, 5},
           {4, 6},
           {5, 7},
           {6, 7}}};
}

TEST(tessellate_mesh_Test, returns_empty_mesh_for_0_grid_size) {
  const auto unit_cell = unit_box();
  const auto grid_size = std::array<std::size_t, 3>{0, 1, 1};
  const auto mesh = tessellate_mesh(unit_cell, grid_size);
  EXPECT_THAT(mesh.positions, ::testing::IsEmpty());
  EXPECT_THAT(mesh.connectivity, ::testing::IsEmpty());
};

TEST(tessellate_mesh_Test, returns_original_mesh_for_1_grid_size) {
  const auto unit_cell = unit_box();
  const auto grid_size = std::array<std::size_t, 3>{1, 1, 1};
  const auto mesh = tessellate_mesh(unit_cell, grid_size);
  EXPECT_EQ(mesh.positions, unit_cell.positions);
  EXPECT_EQ(mesh.connectivity, unit_cell.connectivity);
};

TEST(tessellate_mesh_Test, tesselates_1x1x2_box) {
  const auto unit_cell = unit_box();
  const auto grid_size = std::array<std::size_t, 3>{1, 1, 2};
  const auto mesh = tessellate_mesh(unit_cell, grid_size);

  const auto expected_positions = std::vector<std::array<double, 3>>{
      {0., 0., 0.}, {1., 0., 0.}, {0., 1., 0.}, {1., 1., 0.},
      {0., 0., 1.}, {1., 0., 1.}, {0., 1., 1.}, {1., 1., 1.},
      {0., 0., 2.}, {1., 0., 2.}, {0., 1., 2.}, {1., 1., 2.}};

  const auto expected_connectivity = std::vector<std::array<std::size_t, 2>>{
      {0, 1},  {0, 2},  {1, 3}, {2, 3},  {0, 4},  {1, 5},  {2, 6},
      {3, 7},  {4, 5},  {4, 6}, {5, 7},  {6, 7},  {4, 8},  {5, 9},
      {6, 10}, {7, 11}, {8, 9}, {8, 10}, {9, 11}, {10, 11}};
  EXPECT_THAT(mesh.positions, testing::ContainerEq(expected_positions));
  EXPECT_THAT(mesh.connectivity, testing::ContainerEq(expected_connectivity));
};

Mesh triad() {
  const auto x = 0.5;
  const auto y = 0.25;
  const auto z = 0.125;
  const auto a = 4.0;
  const auto b = 5.0;
  const auto c = 6.0;
  return {{
              {x, y, z},
              {x + a, y, z},
              {x, y + b, z},
              {x, y, z + c},
          },
          {{0, 1}, {0, 2}, {0, 3}}};
}

Mesh expected_triad() {
  const auto p = [](const auto i, const auto j, const auto k) {
    const auto x0 = 0.5;
    const auto y0 = 0.25;
    const auto z0 = 0.125;
    const auto a = 4.0;
    const auto b = 5.0;
    const auto c = 6.0;
    return std::array<double, 3>{x0 + static_cast<double>(i) * a,
                                 y0 + static_cast<double>(j) * b,
                                 z0 + static_cast<double>(k) * c};
  };
  //             ...
  //             | |
  //  2 5    ->  2-5-9
  //  | |        | |
  //  0-1-4      0-1-4
  return {{
              // (0,0,0)
              p(0, 0, 0), // 0
              p(1, 0, 0), // 1
              p(0, 1, 0), // 2
              p(0, 0, 1),
              // (1,0,0)
              p(2, 0, 0), // 4
              p(1, 1, 0), // 5
              p(1, 0, 1),
              // (0,1,0)
              p(0, 2, 0), // 7
              p(0, 1, 1),
              // (1,1,0)
              p(2, 1, 0), // 9
              p(1, 2, 0), // 10
              p(1, 1, 1),
              // (0,2,0)
              p(0, 3, 0), // 12
              p(0, 2, 1),
              // (1,2,0)
              p(2, 2, 0), // 14
              p(1, 3, 0), // 15
              p(1, 2, 1),
              // (0,0,1)
              p(0, 0, 2),
              // (1,0,1)
              p(2, 0, 1), // 18
              p(1, 0, 2),
              // (0,1,1)
              p(0, 1, 2),
              // (1,1,1)
              p(2, 1, 1), // 21
              p(1, 1, 2),
              // (0,2,1)
              p(0, 3, 1), // 23
              p(0, 2, 2),
              // (1,2,1)
              p(2, 2, 1), // 25
              p(1, 3, 1), // 26
              p(1, 2, 2),
          },
          {
              // (0,0,0)
              {0, 1},
              {0, 2},
              {0, 3},
              // (1,0,0)
              {1, 4},
              {1, 5},
              {1, 6},
              // (0,1,0)
              {2, 5}, // (1,1,0)
              {2, 7},
              {2, 8},
              // (1,1,0)
              {5, 9},
              {5, 10},
              {5, 11},
              // (0,2,0)
              {7, 10}, // (1,2,0)
              {7, 12},
              {7, 13},
              // (1,2,0)
              {10, 14},
              {10, 15},
              {10, 16},
              // (0,0,1)
              {3, 6}, // (1,0,1)
              {3, 8}, // (0,1,1)
              {3, 17},
              // (1,0,1)
              {6, 18},
              {6, 11}, // (1,1,1)
              {6, 19},
              // (0,1,1)
              {8, 11}, // (1,1,1)
              {8, 13}, // (0,2,1)
              {8, 20},
              // (1,1,1)
              {11, 21},
              {11, 16}, // (1,2,1)
              {11, 22},
              // (0,2,1)
              {13, 16}, // (1,2,1)
              {13, 23},
              {13, 24},
              // (1,2,1)
              {16, 25},
              {16, 26},
              {16, 27},
          }};
}

TEST(tessellate_mesh_Test, tesselates_2x3x2_triad) {
  const auto unit_cell = triad();
  const auto grid_size = std::array<std::size_t, 3>{2, 3, 2};
  const auto mesh = tessellate_mesh(unit_cell, grid_size);

  const auto expected = expected_triad();

  EXPECT_THAT(mesh.positions, testing::ContainerEq(expected.positions));
  EXPECT_THAT(mesh.connectivity, testing::ContainerEq(expected.connectivity));
};

TEST(tessellate_mesh_Test, tesselates_2x3_square) {
  using Mesh = MeshGeometry<std::array<double, 2>, std::array<std::size_t, 2>>;
  const auto unit_cell = Mesh{{
                                  {0.0, 0.0},
                                  {1.0, 0.0},
                                  {0.0, 1.0},
                                  {1.0, 1.0},
                              },
                              {{0, 1}, {0, 2}, {1, 3}, {2, 3}}};
  const auto grid_size = std::array<std::size_t, 2>{2, 3};
  const auto mesh = tessellate_mesh(unit_cell, grid_size);

  /*
  **   9 10 11
  **   6 7  8
  **   2 3  5
  **   0 1  4
  */
  const auto expected = Mesh{{
                                 {0.0, 0.0},
                                 {1.0, 0.0},
                                 {0.0, 1.0},
                                 {1.0, 1.0},
                                 {2.0, 0.0}, // 4
                                 {2.0, 1.0}, // 5
                                 {0.0, 2.0}, // 6
                                 {1.0, 2.0}, // 7
                                 {2.0, 2.0}, // 8
                                 {0.0, 3.0}, // 9
                                 {1.0, 3.0}, // 10
                                 {2.0, 3.0}, // 11
                             },
                             {
                                 {0, 1},
                                 {0, 2},
                                 {1, 3},
                                 {2, 3},
                                 {1, 4},
                                 {4, 5},
                                 {3, 5},
                                 {2, 6},
                                 {3, 7},
                                 {6, 7},
                                 {7, 8},
                                 {5, 8},
                                 {6, 9},
                                 {9, 10},
                                 {7, 10},
                                 {10, 11},
                                 {8, 11},
                             }};

  EXPECT_THAT(mesh.positions, testing::ContainerEq(expected.positions));
  EXPECT_THAT(mesh.connectivity,
              testing::UnorderedElementsAreArray(expected.connectivity));
};

} // namespace ae108::meshing
