// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/meshing/refine_segment_mesh.h"
#include "ae108/tensor/Tensor.h"
#include <array>
#include <cmath>
#include <gmock/gmock.h>

namespace ae108::meshing {

using Segment = std::array<std::size_t, 2>;
using Mesh = MeshGeometry<std::array<double, 3>, Segment>;

Mesh unit_tetrahedron() {
  return {{
              {0., 0., 0.},
              {1., 0., 0.},
              {0., 1., 0.},
              {0., 0., 1.},
          },
          {
              {0, 1},
              {0, 2},
              {0, 3},
              {1, 2},
              {1, 3},
              {2, 3},
          }};
}

TEST(refine_segment_mesh_Test,
     refine_segment_mesh_by_n_divisions_does_not_change_mesh_for_1_division) {
  auto original_mesh = unit_tetrahedron();
  const auto max_divisions = std::size_t{1};
  const auto mesh =
      refine_segment_mesh_by_n_divisions(original_mesh, max_divisions);
  EXPECT_THAT(mesh.positions, ::testing::ContainerEq(original_mesh.positions));
  EXPECT_THAT(mesh.connectivity,
              ::testing::ContainerEq(original_mesh.connectivity));
};

Mesh expected_refinement() {
  return {{
              {0.0, 0.0, 0.0},
              {1.0, 0.0, 0.0},
              {0.0, 1.0, 0.0},
              {0.0, 0.0, 1.0},
              {0.5, 0.5, 0.0},
              {0.5, 0.0, 0.5},
              {0.0, 0.5, 0.5},
          },
          {
              {0, 1},
              {0, 2},
              {0, 3},
              {1, 4},
              {4, 2},
              {1, 5},
              {5, 3},
              {2, 6},
              {6, 3},
          }};
}

TEST(refine_segment_mesh_Test,
     refine_segment_mesh_by_n_divisions_inserts_segments_for_2_divisions) {
  auto original_mesh = unit_tetrahedron();
  const auto max_divisions = std::size_t{2};
  const auto mesh =
      refine_segment_mesh_by_n_divisions(original_mesh, max_divisions);
  const auto expected_mesh = expected_refinement();
  EXPECT_THAT(mesh.positions, ::testing::ContainerEq(expected_mesh.positions));
  EXPECT_THAT(mesh.connectivity,
              ::testing::ContainerEq(expected_mesh.connectivity));
};

TEST(refine_segment_mesh_Test,
     refine_segment_mesh_by_n_divisions_inserts_segments_for_6_divisions) {
  auto original_mesh = Mesh{{{0.0, 0.0, 0.0}, {6.0, 0.0, 0.0}}, {{0, 1}}};
  const auto max_divisions = std::size_t{6};
  const auto mesh =
      refine_segment_mesh_by_n_divisions(original_mesh, max_divisions);
  const auto expected_mesh =
      Mesh{{{0.0, 0.0, 0.0},
            {6.0, 0.0, 0.0},
            {1.0, 0.0, 0.0},
            {2.0, 0.0, 0.0},
            {3.0, 0.0, 0.0},
            {4.0, 0.0, 0.0},
            {5.0, 0.0, 0.0}},
           {{0, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {6, 1}}};
  EXPECT_THAT(mesh.positions, ::testing::ContainerEq(expected_mesh.positions));
  EXPECT_THAT(mesh.connectivity,
              ::testing::ContainerEq(expected_mesh.connectivity));
};

TEST(refine_segment_mesh_Test, does_not_change_mesh_for_no_refinement_2D) {
  using Point = tensor::Tensor<double, 2>;
  const auto unrefined_mesh =
      MeshGeometry<Point, Segment>{{{0., 0.}, {1., 0.}}, {{0, 1}}};

  const auto refined_mesh = refine_segment_mesh(unrefined_mesh, 2.0);

  EXPECT_THAT(refined_mesh.positions,
              ::testing::ContainerEq(unrefined_mesh.positions));
  EXPECT_THAT(refined_mesh.connectivity,
              ::testing::ContainerEq(unrefined_mesh.connectivity));
}

TEST(refine_segment_mesh_Test, does_not_change_mesh_for_no_refinement_3D) {
  using Point = tensor::Tensor<double, 3>;
  const auto unrefined_mesh =
      MeshGeometry<Point, Segment>{{{0., 0., 0.}, {1., 0., 0.}}, {{0, 1}}};

  const auto refined_mesh = refine_segment_mesh(unrefined_mesh, 2.0);

  EXPECT_THAT(refined_mesh.positions,
              ::testing::ContainerEq(unrefined_mesh.positions));
  EXPECT_THAT(refined_mesh.connectivity,
              ::testing::ContainerEq(unrefined_mesh.connectivity));
}

TEST(refine_segment_mesh_Test, returns_correct_mesh_for_single_refinement_2D) {
  using Point = tensor::Tensor<double, 2>;
  const auto unrefined_mesh =
      MeshGeometry<Point, Segment>{{{0., 0.}, {1., 0.}}, {{0, 1}}};

  const auto refined_mesh = refine_segment_mesh(unrefined_mesh, 0.6);

  const auto expected_mesh = MeshGeometry<Point, Segment>{
      {{0.0, 0.0}, {1.0, 0.0}, {0.5, 0.0}}, {{0, 2}, {2, 1}}};

  EXPECT_THAT(refined_mesh.positions,
              ::testing::ContainerEq(expected_mesh.positions));
  EXPECT_THAT(refined_mesh.connectivity,
              ::testing::ContainerEq(expected_mesh.connectivity));
}

TEST(refine_segment_mesh_Test, returns_correct_mesh_for_double_refinement_2D) {

  using Point = tensor::Tensor<double, 2>;
  const auto unrefined_mesh =
      MeshGeometry<Point, Segment>{{{0., 0.}, {1., 0.}}, {{0, 1}}};

  const auto refined_mesh = refine_segment_mesh(unrefined_mesh, 0.4);

  const auto expected_mesh = MeshGeometry<Point, Segment>{
      {{0.0, 0.0}, {1.0, 0.0}, {1. / 3, 0.0}, {2. / 3, 0.0}},
      {{0, 2}, {2, 3}, {3, 1}}};

  EXPECT_THAT(refined_mesh.positions,
              ::testing::ContainerEq(expected_mesh.positions));
  EXPECT_THAT(refined_mesh.connectivity,
              ::testing::ContainerEq(expected_mesh.connectivity));
}

TEST(refine_segment_mesh_Test, returns_correct_mesh_for_single_refinement_3D) {

  using Point = tensor::Tensor<double, 3>;
  const auto unrefined_mesh =
      MeshGeometry<Point, Segment>{{{0., 0., 0.}, {1., 0., 0.}}, {{0, 1}}};

  const auto refined_mesh = refine_segment_mesh(unrefined_mesh, 0.6);

  const auto expected_mesh = MeshGeometry<Point, Segment>{
      {{0.0, 0.0, 0.0}, {1.0, 0.0, 0.0}, {0.5, 0.0, 0.0}}, {{0, 2}, {2, 1}}};

  EXPECT_THAT(refined_mesh.positions,
              ::testing::ContainerEq(expected_mesh.positions));
  EXPECT_THAT(refined_mesh.connectivity,
              ::testing::ContainerEq(expected_mesh.connectivity));
}

TEST(refine_segment_mesh_Test,
     returns_correct_number_of_segments_and_points_for_complex_refinement_2D) {

  using Point = tensor::Tensor<double, 2>;
  const auto unrefined_mesh =
      MeshGeometry<Point, Segment>{{{0., 0.}, {1., 0.}, {1., 1.}, {0., 1.}},
                                   {{0, 1}, {1, 2}, {2, 3}, {3, 0}}};

  const auto refined_mesh = refine_segment_mesh(unrefined_mesh, 0.1);

  EXPECT_THAT(refined_mesh.connectivity, testing::SizeIs(40));
  EXPECT_THAT(refined_mesh.positions, testing::SizeIs(40));
}

} // namespace ae108::meshing
