// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of ae108.
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/meshing/generate_tetrahedron_mesh.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using testing::DoubleEq;
using testing::ElementsAre;
using testing::IsEmpty;

namespace ae108::meshing {
namespace {

TEST(generate_tetrahedron_mesh_Test,
     returns_empty_mesh_for_zero_x_granularity) {
  const auto mesh = generate_tetrahedron_mesh({{1., 1., 1.}}, {{0, 1, 1}});

  EXPECT_THAT(mesh.positions, IsEmpty());
  EXPECT_THAT(mesh.connectivity, IsEmpty());
}

TEST(generate_tetrahedron_mesh_Test,
     returns_empty_mesh_for_zero_y_granularity) {
  const auto mesh = generate_tetrahedron_mesh({{1., 1., 1.}}, {{1, 0, 1}});

  EXPECT_THAT(mesh.positions, IsEmpty());
  EXPECT_THAT(mesh.connectivity, IsEmpty());
}

TEST(generate_tetrahedron_mesh_Test,
     returns_empty_mesh_for_zero_z_granularity) {
  const auto mesh = generate_tetrahedron_mesh({{1., 1., 1.}}, {{1, 1, 0}});

  EXPECT_THAT(mesh.positions, IsEmpty());
  EXPECT_THAT(mesh.connectivity, IsEmpty());
}

TEST(generate_tetrahedron_mesh_Test, has_correct_mesh_for_cube) {
  const auto mesh = generate_tetrahedron_mesh({{1., 1., 1.}}, {{1, 1, 1}});

  EXPECT_THAT(
      mesh.positions,
      ElementsAre(ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(1.))));
  EXPECT_THAT(mesh.connectivity,
              ElementsAre(ElementsAre(0, 1, 2, 4), ElementsAre(1, 7, 4, 5),
                          ElementsAre(1, 2, 7, 3), ElementsAre(7, 2, 4, 6),
                          ElementsAre(7, 1, 4, 2)));
}

TEST(generate_tetrahedron_mesh_Test, has_correct_mesh_for_x_scaled_cube) {
  const auto mesh = generate_tetrahedron_mesh({{2., 1., 1.}}, {{1, 1, 1}});

  EXPECT_THAT(
      mesh.positions,
      ElementsAre(ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(2.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(2.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(2.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(2.), DoubleEq(1.), DoubleEq(1.))));

  EXPECT_THAT(mesh.connectivity,
              ElementsAre(ElementsAre(0, 1, 2, 4), ElementsAre(1, 7, 4, 5),
                          ElementsAre(1, 2, 7, 3), ElementsAre(7, 2, 4, 6),
                          ElementsAre(7, 1, 4, 2)));
}

TEST(generate_tetrahedron_mesh_Test, has_correct_mesh_for_y_scaled_cube) {
  const auto mesh = generate_tetrahedron_mesh({{1., 2., 1.}}, {{1, 1, 1}});

  EXPECT_THAT(
      mesh.positions,
      ElementsAre(ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(2.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(2.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(2.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(2.), DoubleEq(1.))));

  EXPECT_THAT(mesh.connectivity,
              ElementsAre(ElementsAre(0, 1, 2, 4), ElementsAre(1, 7, 4, 5),
                          ElementsAre(1, 2, 7, 3), ElementsAre(7, 2, 4, 6),
                          ElementsAre(7, 1, 4, 2)));
}

TEST(generate_tetrahedron_mesh_Test, has_correct_mesh_for_z_scaled_cube) {
  const auto mesh = generate_tetrahedron_mesh({{1., 1., 2.}}, {{1, 1, 1}});

  EXPECT_THAT(
      mesh.positions,
      ElementsAre(ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(2.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(2.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(2.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(2.))));

  EXPECT_THAT(mesh.connectivity,
              ElementsAre(ElementsAre(0, 1, 2, 4), ElementsAre(1, 7, 4, 5),
                          ElementsAre(1, 2, 7, 3), ElementsAre(7, 2, 4, 6),
                          ElementsAre(7, 1, 4, 2)));
}

TEST(generate_tetrahedron_mesh_Test, has_correct_mesh_for_x_split_cuboid) {
  const auto mesh = generate_tetrahedron_mesh({{2., 1., 1.}}, {{2, 1, 1}});

  EXPECT_THAT(
      mesh.positions,
      ElementsAre(ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(2.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(2.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(2.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(2.), DoubleEq(1.), DoubleEq(1.))));

  EXPECT_THAT(mesh.connectivity,
              ElementsAre(ElementsAre(0, 1, 3, 6), ElementsAre(1, 10, 6, 7),
                          ElementsAre(1, 3, 10, 4), ElementsAre(10, 3, 6, 9),
                          ElementsAre(10, 1, 6, 3), ElementsAre(1, 2, 5, 8),
                          ElementsAre(5, 10, 8, 11), ElementsAre(1, 10, 5, 4),
                          ElementsAre(1, 8, 10, 7), ElementsAre(8, 10, 5, 1)));
}

TEST(generate_tetrahedron_mesh_Test, has_correct_mesh_for_y_split_cuboid) {
  const auto mesh = generate_tetrahedron_mesh({{1., 2., 1.}}, {{1, 2, 1}});

  EXPECT_THAT(
      mesh.positions,
      ElementsAre(ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(2.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(2.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(2.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(2.), DoubleEq(1.))));

  EXPECT_THAT(mesh.connectivity,
              ElementsAre(ElementsAre(0, 1, 2, 6), ElementsAre(1, 9, 6, 7),
                          ElementsAre(1, 2, 9, 3), ElementsAre(9, 2, 6, 8),
                          ElementsAre(9, 1, 6, 2), ElementsAre(2, 3, 5, 9),
                          ElementsAre(5, 10, 9, 11), ElementsAre(2, 10, 5, 4),
                          ElementsAre(2, 9, 10, 8), ElementsAre(9, 10, 5, 2)));
}

TEST(generate_tetrahedron_mesh_Test, has_correct_mesh_for_z_split_cuboid) {
  const auto mesh = generate_tetrahedron_mesh({{1., 1., 2.}}, {{1, 1, 2}});

  EXPECT_THAT(
      mesh.positions,
      ElementsAre(ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(0.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(1.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(0.), DoubleEq(2.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(0.), DoubleEq(2.)),
                  ElementsAre(DoubleEq(0.), DoubleEq(1.), DoubleEq(2.)),
                  ElementsAre(DoubleEq(1.), DoubleEq(1.), DoubleEq(2.))));

  EXPECT_THAT(mesh.connectivity,
              ElementsAre(ElementsAre(0, 1, 2, 4), ElementsAre(1, 7, 4, 5),
                          ElementsAre(1, 2, 7, 3), ElementsAre(7, 2, 4, 6),
                          ElementsAre(7, 1, 4, 2), ElementsAre(4, 5, 7, 9),
                          ElementsAre(7, 10, 9, 11), ElementsAre(4, 10, 7, 6),
                          ElementsAre(4, 9, 10, 8), ElementsAre(9, 10, 7, 4)));
}

} // namespace
} // namespace ae108::meshing
