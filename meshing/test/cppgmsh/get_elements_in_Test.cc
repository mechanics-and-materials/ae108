// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/meshing/cppgmsh/Context.h"
#include "ae108/meshing/cppgmsh/construct_box.h"
#include "ae108/meshing/cppgmsh/generate_mesh.h"
#include "ae108/meshing/cppgmsh/get_elements_in.h"
#include "ae108/meshing/cppgmsh/get_nodes_of.h"
#include "ae108/meshing/cppgmsh/set_granularity.h"
#include <cstddef>
#include <gmock/gmock.h>
#include <gmsh.h>
#include <gtest/gtest.h>
#include <optional>
#include <vector>

using testing::ElementsAre;
using testing::Ge;
using testing::Le;

namespace ae108::meshing::cppgmsh {

struct get_elements_in_Test : testing::Test {};

TEST_F(get_elements_in_Test, returns_correct_number_of_tets_for_simple_box) {
  const auto gmshContext = Context(0, nullptr);

  construct_box({0., 0., 0.}, {1., 1., 1.});
  gmsh::model::occ::synchronize();

  set_granularity(1);
  generate_mesh(3, 1, 6);

  auto elements = get_elements_in({3, -1});
  EXPECT_EQ(elements.size(), 24);
  EXPECT_EQ(elements[0].first, 4);
}

std::optional<Node<3>> find_node(const auto &nodes, const auto node_id) {
  for (const auto &candidate : nodes) {
    if (node_id == candidate.id) {
      return std::optional{candidate};
    }
  }
  return std::nullopt;
}

auto prepare_id_to_pos(const auto &elements) {
  auto element_ids = std::unordered_map<unsigned int, std::size_t>{};
  auto pos = std::size_t{0};
  for (const auto &element : elements) {
    element_ids.insert({element.second, pos++});
  }
  return element_ids;
}

TEST_F(get_elements_in_Test, returns_the_correct_elements) {
  const auto gmshContext = Context(0, nullptr);

  const auto box_1 = construct_box({0., 0., 0.}, {1., 1., 1.});
  const auto box_2 = construct_box({10., 10., 10.}, {1., 1., 1.});
  gmsh::model::occ::synchronize();

  set_granularity(1.);
  generate_mesh(3, 1, 6);

  auto elements = get_elements_in({3, -1});
  std::vector<std::size_t> foundElement(elements.size(), std::size_t{0});
  const auto id_to_pos = prepare_id_to_pos(elements);

  auto elements_1 = get_elements_in({3, box_1.second});
  auto elements_2 = get_elements_in({3, box_2.second});

  const auto all_nodes = get_nodes_of<3>({3, -1});
  for (auto &i : elements_1) {
    int type{};
    std::vector<std::size_t> nodes;
#if GMSH_API_VERSION_MINOR <= 8
    gmsh::model::mesh::getElement(i.second, type, nodes);
#else
    {
      auto dim = int{};
      auto tag = int{};
      gmsh::model::mesh::getElement(i.second, type, nodes, dim, tag);
    }
#endif
    EXPECT_EQ(i.first, type);
    for (unsigned long const node_id : nodes) {
      const auto &node = find_node(all_nodes, node_id);
      EXPECT_TRUE(node.has_value());
      if (node.has_value()) {
        EXPECT_THAT(node.value().position, ElementsAre(Ge(0.), Ge(0.), Ge(0.)));
        EXPECT_THAT(node.value().position, ElementsAre(Le(1.), Le(1.), Le(1.)));
      }
    }
    if (id_to_pos.find(i.second) != id_to_pos.end()) {
      foundElement[id_to_pos.at(i.second)]++;
    }
  }

  for (auto &i : elements_2) {
    int type{};
    std::vector<std::size_t> nodes;
#if GMSH_API_VERSION_MINOR <= 8
    gmsh::model::mesh::getElement(i.second, type, nodes);
#else
    {
      auto dim = int{};
      auto tag = int{};
      gmsh::model::mesh::getElement(i.second, type, nodes, dim, tag);
    }
#endif
    for (unsigned long const node : nodes) {
      bool foundNode = false;
      for (const auto &all_node : all_nodes) {
        if (node == all_node.id) {
          foundNode = true;
          EXPECT_THAT(all_node.position,
                      ElementsAre(Ge(10.), Ge(10.), Ge(10.)));
          EXPECT_THAT(all_node.position,
                      ElementsAre(Le(11.), Le(11.), Le(11.)));
          break;
        }
      }
      EXPECT_TRUE(foundNode);
    }
    if (id_to_pos.find(i.second) != id_to_pos.end()) {
      foundElement[id_to_pos.at(i.second)]++;
    }
  }
  // Each element should have been found exaclty one time:
  EXPECT_THAT(foundElement, testing::Each(1));
}

} // namespace ae108::meshing::cppgmsh
