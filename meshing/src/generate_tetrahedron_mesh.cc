// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of ae108.
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/meshing/generate_tetrahedron_mesh.h"
#include <array>
#include <functional>
#include <numeric>

namespace ae108::meshing {

namespace {

using Point = std::array<double, 3>;
using Cell = std::array<std::size_t, 4>;
using size_type = std::size_t;
using Index = std::array<size_type, 3>;

/**
 * @brief Computes the number of smaller cuboids the cuboid
 * will be split into.
 */
size_type number_of_cuboids(const Index &granularity) noexcept {
  return std::accumulate(granularity.begin(), granularity.end(), size_type{1},
                         std::multiplies<size_type>{});
}

/**
 * @brief Converts the steps in x, y, and z
 * direction to the ID of the point at that location.
 */
size_type steps_to_id(const Index &steps, const Index &granularity) noexcept {
  return steps[2] * (granularity[1] + size_type{1}) *
             (granularity[0] + size_type{1}) +
         steps[1] * (granularity[0] + size_type{1}) + steps[0];
}

/**
 * @brief Converts the steps in x, y, and z
 * direction to the position of the point at that location.
 */
Point steps_to_position(const Index &steps, const Point &size,
                        const Index &granularity) noexcept {
  return {{
      static_cast<double>(steps[0]) * static_cast<double>(size[0]) /
          static_cast<double>(granularity[0]),
      static_cast<double>(steps[1]) * static_cast<double>(size[1]) /
          static_cast<double>(granularity[1]),
      static_cast<double>(steps[2]) * static_cast<double>(size[2]) /
          static_cast<double>(granularity[2]),
  }};
}

/**
 * @brief Generates the positions of the points in the mesh.
 */
std::vector<Point> generate_positions(const Point &size,
                                      const Index &granularity) noexcept {
  auto positions = std::vector<Point>{};
  positions.reserve(number_of_cuboids(granularity));

  auto steps = Index{};
  for (steps[2] = 0; steps[2] <= granularity[2]; ++steps[2])
    for (steps[1] = 0; steps[1] <= granularity[1]; ++steps[1])
      for (steps[0] = 0; steps[0] <= granularity[0]; ++steps[0])
        positions.emplace_back(steps_to_position(steps, size, granularity));

  return positions;
}

/**
 * @brief Generates the connectivity of the mesh.
 */
std::vector<Cell> generate_connectivity(const Index &granularity) noexcept {
  auto connectivity = std::vector<Cell>{};

  constexpr auto tetrahedra_per_cuboid = size_type{5};
  constexpr auto points_per_tetrahedron = size_type{4};

  connectivity.reserve(number_of_cuboids(granularity) * tetrahedra_per_cuboid);

  using ElementVertexSteps =
      std::array<std::array<Index, points_per_tetrahedron>,
                 tetrahedra_per_cuboid>;
  constexpr ElementVertexSteps quintet_A = {{
      {{{{0, 0, 0}}, {{1, 0, 0}}, {{0, 1, 0}}, {{0, 0, 1}}}},
      {{{{1, 0, 0}}, {{1, 1, 1}}, {{0, 0, 1}}, {{1, 0, 1}}}},
      {{{{1, 0, 0}}, {{0, 1, 0}}, {{1, 1, 1}}, {{1, 1, 0}}}},
      {{{{1, 1, 1}}, {{0, 1, 0}}, {{0, 0, 1}}, {{0, 1, 1}}}},
      {{{{1, 1, 1}}, {{1, 0, 0}}, {{0, 0, 1}}, {{0, 1, 0}}}},
  }};
  constexpr ElementVertexSteps quintet_B = {{
      {{{{0, 0, 0}}, {{1, 0, 0}}, {{1, 1, 0}}, {{1, 0, 1}}}},
      {{{{1, 1, 0}}, {{0, 1, 1}}, {{1, 0, 1}}, {{1, 1, 1}}}},
      {{{{0, 0, 0}}, {{0, 1, 1}}, {{1, 1, 0}}, {{0, 1, 0}}}},
      {{{{0, 0, 0}}, {{1, 0, 1}}, {{0, 1, 1}}, {{0, 0, 1}}}},
      {{{{1, 0, 1}}, {{0, 1, 1}}, {{1, 1, 0}}, {{0, 0, 0}}}},
  }};

  const auto add_quintet = [&](const ElementVertexSteps &quintet,
                               const Index &offset) {
    for (auto &&element : quintet) {
      auto cell = Cell{};
      for (std::size_t i = 0; i < cell.size(); i++) {
        const auto &steps = element[i];
        cell[i] = steps_to_id({{
                                  steps[0] + offset[0],
                                  steps[1] + offset[1],
                                  steps[2] + offset[2],
                              }},
                              granularity);
      }
      connectivity.emplace_back(cell);
    }
  };

  auto offset = Index();
  for (offset[2] = 0; offset[2] < granularity[2]; ++offset[2])
    for (offset[1] = 0; offset[1] < granularity[1]; ++offset[1])
      for (offset[0] = 0; offset[0] < granularity[0]; ++offset[0]) {
        const auto &quintet = ((offset[0] + offset[1] + offset[2]) % 2) != 0U
                                  ? quintet_B
                                  : quintet_A;
        add_quintet(quintet, offset);
      }

  return connectivity;
}

} // namespace

MeshGeometry<Point, Cell> generate_tetrahedron_mesh(
    const std::array<double, 3> &size,
    const std::array<std::size_t, 3> &granularity) noexcept {
  if (number_of_cuboids(granularity) == 0) {
    return {{}, {}};
  }

  return {generate_positions(size, granularity),
          generate_connectivity(granularity)};
}

} // namespace ae108::meshing
