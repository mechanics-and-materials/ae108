// © 2020 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of ae108.
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/meshing/generate_triangle_mesh.h"
#include "ae108/meshing/MeshGeometry.h"
#include <cstddef>

namespace ae108::meshing {

MeshGeometry<std::array<double, 2>, std::array<std::size_t, 3>>
generate_triangle_mesh(const std::array<double, 2> &size,
                       const std::array<std::size_t, 2> &granularity) noexcept {
  using size_type = std::size_t;
  using Point = std::array<double, 2>;

  if (granularity[0] * granularity[1] == 0) {
    return {{}, {}};
  }

  const auto generate_positions = [&]() {
    auto positions = std::vector<Point>{};
    positions.reserve((granularity[0] + 1) * (granularity[1] + 1));

    auto step = std::array<size_type, 2>{};
    for (step[0] = 0; step[0] < granularity[0] + 1; ++step[0]) {
      for (step[1] = 0; step[1] < granularity[1] + 1; ++step[1]) {
        positions.push_back({{
            (static_cast<double>(step[0]) * size[0]) /
                static_cast<double>(granularity[0]),
            (static_cast<double>(step[1]) * size[1]) /
                static_cast<double>(granularity[1]),
        }});
      }
    }

    return positions;
  };

  const auto step_to_index = [&](const size_type step_x,
                                 const size_type step_y) {
    return step_x * (granularity[1] + 1) + step_y;
  };

  const auto generate_connectivity = [&]() {
    auto connectivity = std::vector<std::array<size_type, 3>>{};
    connectivity.reserve(2 * granularity[0] * granularity[1]);

    auto step = std::array<size_type, 2>{{0, 0}};
    for (step[0] = 0; step[0] < granularity[0]; ++step[0]) {
      for (step[1] = 0; step[1] < granularity[1]; ++step[1]) {
        connectivity.push_back({{
            step_to_index(step[0], step[1]),
            step_to_index(step[0] + 1, step[1]),
            step_to_index(step[0], step[1] + 1),
        }});
        connectivity.push_back({{
            step_to_index(step[0] + 1, step[1]),
            step_to_index(step[0] + 1, step[1] + 1),
            step_to_index(step[0], step[1] + 1),
        }});
      }
    }

    return connectivity;
  };

  return {generate_positions(), generate_connectivity()};
}

} // namespace ae108::meshing
