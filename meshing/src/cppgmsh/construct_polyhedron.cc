// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

// NOLINTNEXTLINE(misc-include-cleaner)
#include "ae108/meshing/cppgmsh/construct_polyhedron.h"
#include "ae108/meshing/BoundaryRepresentation.h"
#include <cstddef>
#include <gmsh.h>
#include <map>
#include <utility>
#include <vector>

namespace ae108::meshing::cppgmsh {

template <>
std::pair<int, int> construct_polyhedron(
    const BoundaryRepresentation<std::size_t, double, 3> &brep) noexcept {

  std::map<std::size_t, int> point2tag;
  for (std::size_t index = 0; index < brep.vertices.size(); index++) {
    const auto &vertex = brep.vertices[index];
    point2tag[index] =
        gmsh::model::occ::addPoint(vertex[0], vertex[1], vertex[2]);
  }

  std::map<std::size_t, int> edge2tag;
  for (std::size_t index = 0; index < brep.edges.size(); index++) {
    const auto &edge = brep.edges[index];
    edge2tag[index] =
        gmsh::model::occ::addLine(point2tag.at(edge[0]), point2tag.at(edge[1]));
  }

  const auto map_face = [&edge2tag](const auto &face) {
    auto mapped_face = std::vector<int>{};
    mapped_face.reserve(face.size());
    for (const auto edge : face)
      mapped_face.emplace_back(edge2tag.at(edge));
    return mapped_face;
  };

  std::vector<int> surfaceTags;
  for (const auto &line_loop : brep.faces)
    surfaceTags.push_back(gmsh::model::occ::addPlaneSurface(
        {gmsh::model::occ::addCurveLoop(map_face(line_loop))}));

  return {3, gmsh::model::occ::addVolume(
                 {gmsh::model::occ::addSurfaceLoop(surfaceTags)})};
}

} // namespace ae108::meshing::cppgmsh
