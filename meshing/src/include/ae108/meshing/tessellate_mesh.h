// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "ae108/meshing/MeshGeometry.h"
#include "ae108/meshing/bounding_box_of.h"
#include <algorithm>
#include <array>
#include <cmath>
#include <vector>

namespace ae108::meshing {

namespace detail {

template <class Cell_>
bool contains_element(const std::vector<Cell_> &connectivity,
                      const Cell_ &cell) {
  const auto element_eq = [](const auto &a, const auto &b) {
    if (a.size() != b.size())
      return false;
    for (std::size_t i = 0; i < a.size(); i++)
      if (a[i] != b[i])
        return false;
    return true;
  };
  for (const auto &element : connectivity) {
    auto sorted = element;
    std::sort(sorted.begin(), sorted.end());
    if (element_eq(cell, sorted))
      return true;
  }
  return false;
}

/**
 * @brief p ~= q - delta e_{dim}
 */
template <class Point>
bool is_periodic_image(const Point &p, const Point &q, const double delta,
                       const std::size_t dim, const double spatial_tolerance) {
  for (std::size_t d = 0; d < p.size(); d++) {
    const auto dist = p[d] - q[d] + ((d == dim) ? delta : 0.);
    if (std::abs(dist) > spatial_tolerance)
      return false;
  }
  return true;
};

template <class Cell_>
bool has_no_image(const Cell_ &cell, const auto &original_connectivity,
                  const auto &translation_map) {
  // First check, if one of the nodes in the element does not yet have its
  // image in the mesh:
  for (const auto node : cell) {
    if (translation_map[node] == node) {
      return true;
    }
  }
  // Second: if all the nodes in the elements already have an image, it is
  // still possible that the images are not yet connected:
  auto mirrored = cell;
  for (std::size_t i = 0; i < cell.size(); i++) {
    mirrored[i] = translation_map[mirrored[i]];
  }
  std::sort(mirrored.begin(), mirrored.end());
  return !contains_element(original_connectivity, mirrored);
}

template <class SizeT>
std::vector<SizeT>
build_translation_map(const auto &positions, const double delta,
                      const std::size_t dim, const double spatial_tolerance) {
  auto translation_map = std::vector<SizeT>{};
  translation_map.reserve(positions.size());

  for (std::size_t node_id = 0; node_id < positions.size(); node_id++) {
    const auto &node_pos = positions[node_id];
    auto image_id = node_id;
    for (std::size_t query_node = 0; query_node < positions.size();
         query_node++) {
      //  node_pos == original_positions[query_node] -
      //  tessellation_vector[dim]* e_{dim}
      if (detail::is_periodic_image(node_pos, positions[query_node], delta, dim,
                                    spatial_tolerance)) {
        image_id = query_node;
        break;
      }
    }
    translation_map.push_back(image_id);
  }
  return translation_map;
}

void translate_translation_map(auto &translation_map,
                               const std::size_t n_nodes) {
  const auto old_translation_map = translation_map;
  for (std::size_t node_id = 0; node_id < translation_map.size(); node_id++) {
    if (static_cast<std::size_t>(translation_map[node_id]) <
        translation_map.size())
      translation_map[node_id] = old_translation_map[translation_map[node_id]];
    else
      translation_map[node_id] += n_nodes;
  }
}

template <class Point>
Point box_extents(const BoundingBox<Point> &bounding_box) {
  auto extents = Point{};
  for (std::size_t i = 0; i < bounding_box.min.size(); i++)
    extents[i] = bounding_box.max[i] - bounding_box.min[i];
  return extents;
}

auto translate_element(const auto &element, const auto &translation_map) {
  auto translated_element = element;
  for (std::size_t i = 0; i < translated_element.size(); i++) {
    translated_element[i] = translation_map[translated_element[i]];
  }
  return translated_element;
}

} // namespace detail

/**
 * @brief Tessellate a unit cell of a mesh given by node positions and
 * connectivity.
 */
template <class Point_, class Cell_>
MeshGeometry<Point_, Cell_> tessellate_mesh(
    const MeshGeometry<Point_, Cell_> &mesh,
    const std::array<std::size_t, std::tuple_size<Point_>{}> &grid_size,
    const double spatial_tolerance = 1e-3) {
  constexpr std::size_t Dimension = std::tuple_size<Point_>{};
  using SizeT = Cell_::value_type;

  for (const auto size : grid_size)
    if (size == 0)
      return {{}, {}};
  auto tessellated = mesh;
  const auto tessellation_vector =
      detail::box_extents(bounding_box_of(mesh.positions));

  for (std::size_t dim = 0; dim < Dimension; dim++) {
    auto original = tessellated;

    // Maps node ids from the unit cell to ids of translated nodes:
    auto translation_map = detail::build_translation_map<SizeT>(
        original.positions, tessellation_vector[dim], dim, spatial_tolerance);

    auto repeating_elements_in_mesh = std::vector<SizeT>{};
    for (std::size_t element_id = 0; element_id < original.connectivity.size();
         element_id++) {
      const auto &cell = original.connectivity[element_id];
      if (detail::has_no_image(cell, original.connectivity, translation_map)) {
        repeating_elements_in_mesh.push_back(element_id);
      }
    }

    auto repeating_nodes_in_mesh = std::vector<std::size_t>{};
    auto next_new_node = original.positions.size();
    for (std::size_t node_id = 0; node_id < original.positions.size();
         node_id++) {
      if (translation_map[node_id] == static_cast<SizeT>(node_id)) {
        repeating_nodes_in_mesh.push_back(node_id);
        translation_map[node_id] = next_new_node++;
      }
    }

    for (std::size_t offset = 1; offset < grid_size[dim]; offset++) {
      for (const auto node_id : repeating_nodes_in_mesh) {
        auto new_pos = original.positions[node_id];
        new_pos[dim] += static_cast<double>(offset) * tessellation_vector[dim];
        tessellated.positions.emplace_back(new_pos);
      }

      for (const auto repeating_element_id : repeating_elements_in_mesh) {
        tessellated.connectivity.emplace_back(detail::translate_element(
            original.connectivity[repeating_element_id], translation_map));
      }
      detail::translate_translation_map(translation_map,
                                        repeating_nodes_in_mesh.size());
    }
  }
  return tessellated;
}

} // namespace ae108::meshing
