// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "MeshGeometry.h"
#include "ae108/meshing/MeshGeometry.h"
#include "ae108/tensor/as_vector.h"
#include "ae108/tensor/to_array.h"
#include <cmath>

namespace ae108::meshing {

namespace detail {
template <class Point_, class Cell_, class DivisionsCallback_>
MeshGeometry<Point_, Cell_>
refine_segment_mesh(const MeshGeometry<Point_, Cell_> &mesh,
                    const DivisionsCallback_ divisions_from_d) noexcept {
  using SizeT = Cell_::value_type;

  auto refined = MeshGeometry<Point_, Cell_>{mesh.positions};

  for (const auto [a, b] : mesh.connectivity) {
    const auto source = tensor::as_vector(mesh.positions[a]);
    const auto target = tensor::as_vector(mesh.positions[b]);
    const auto delta = (target - source).eval();
    const auto divisions = divisions_from_d(delta.norm());
    const auto pos_increment = delta / static_cast<double>(divisions);

    auto segment_start = a;
    for (std::size_t segment_no = 1; segment_no < divisions; segment_no++) {
      // add the connection between the new node and the previous node:
      const auto segment_end = static_cast<SizeT>(refined.positions.size());
      refined.connectivity.push_back({segment_start, segment_end});
      // create new node:
      refined.positions.emplace_back(
          tensor::to_array(source + pos_increment * segment_no));
      segment_start = segment_end;
    }
    refined.connectivity.push_back({segment_start, b});
  }
  return refined;
}

} // namespace detail

/**
 * @brief Refines a given mesh of segments. Each segment is refined into smaller
 * segments of equal length such that the longest original segment gets divided
 * into `max_divisions` subsegments (of length `min_length`).
 * Shorter segments will be refined into `n <= max_divisions` subsegments of
 * equal length `>= min_length` for `n` as large as possible.
 *
 * @param mesh The unrefined segment mesh.
 * @param max_divisions The maximum allowed number of subsegments.
 */
template <class Point_, class Cell_>
MeshGeometry<Point_, Cell_>
refine_segment_mesh_by_n_divisions(const MeshGeometry<Point_, Cell_> &mesh,
                                   std::size_t max_divisions) noexcept {
  // Find minimum length of a division segment by applying
  // max_divisions to the longest bar in the mesh
  auto min_segment_length = 0.0;
  for (const auto [a, b] : mesh.connectivity) {
    // Find the bar length upon full division
    const auto as_vec = [](const auto &a) { return tensor::as_vector(a); };
    const auto d_ab =
        (as_vec(mesh.positions[a]) - as_vec(mesh.positions[b])).norm();
    min_segment_length = std::max(min_segment_length, d_ab);
  }
  min_segment_length /= static_cast<double>(max_divisions);

  const auto divisions_by_d = [min_segment_length](const auto delta) {
    return std::max(std::size_t{1}, static_cast<std::size_t>(
                                        floor(delta / min_segment_length)));
  };
  return detail::refine_segment_mesh(mesh, divisions_by_d);
}

/**
 * @brief Refines a given mesh of segments. Each segment is refined into smaller
 * segments of equal length such that no segment is longer than
 * `max_segment_length`.
 *
 * @param mesh The unrefined segment mesh.
 * @param max_segment_length The maximum allowed segment length.
 */
template <class Point_, class Cell_>
MeshGeometry<Point_, Cell_>
refine_segment_mesh(const MeshGeometry<Point_, Cell_> &mesh,
                    const double max_segment_length) noexcept {
  const auto divisions_by_d = [max_segment_length](const auto delta) {
    return std::ceil(delta / max_segment_length);
  };
  return detail::refine_segment_mesh(mesh, divisions_by_d);
}

} // namespace ae108::meshing
