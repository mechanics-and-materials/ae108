// © 2020 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of ae108.
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "ae108/meshing/MeshGeometry.h"
#include <array>

namespace ae108::meshing {

/**
 * @brief Creates a simple triangulation of a rectangle. For instance, it
 * creates the following triangulation for granularity [2, 1]:
 *
 *  *-----*-----*
 *  *  \  |  \  |
 *  *-----*-----*
 *
 * The function generates quadratic triangles. In particular, it uses
 * the following order for the nodes:
 *
 * 2
 * | \
 * 5   4
 * |    \
 * 0--3--1
 *
 * @param size The top-right coordinate of the rectangle. The bottom-left
 * coordinate of the rectangle is [0., 0.].
 * @param granularity The number of pairs of triangles along the two axes.
 */
MeshGeometry<std::array<double, 2>, std::array<std::size_t, 6>>
generate_quadratic_triangle_mesh(
    const std::array<double, 2> &size,
    const std::array<std::size_t, 2> &granularity) noexcept;

} // namespace ae108::meshing
