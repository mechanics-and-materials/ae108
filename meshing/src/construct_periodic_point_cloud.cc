// © 2021 ETH Zurich, Mechanics and Materials Lab
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

// NOLINTNEXTLINE(misc-include-cleaner)
#include "ae108/meshing/construct_periodic_point_cloud.h"
#include <Eigen/Dense>
#include <array>
#include <cstddef>
#include <vector>

namespace ae108::meshing {

namespace {

template <std::size_t Dimension>
std::array<double, Dimension>
transformed_point(const std::array<double, Dimension> &origin,
                  const std::array<double, Dimension> &translation,
                  const std::array<double, Dimension> &preimage) {
  auto point = std::array<double, Dimension>{};
  Eigen::Map<Eigen::Matrix<double, Dimension, 1>>(point.data()) =
      Eigen::Map<const Eigen::Matrix<double, Dimension, 1>>(origin.data()) +
      (Eigen::Map<
           const Eigen::Matrix<double, Dimension, Dimension, Eigen::ColMajor>>(
           translation.data()) *
       Eigen::Map<const Eigen::Matrix<double, Dimension, 1>>(preimage.data()));
  return point;
}

} // namespace

template <>
std::vector<std::array<double, 3>> construct_periodic_point_cloud(
    const std::array<std::array<double, 3>, 3> &translations,
    std::array<double, 3> origin, const std::vector<double> &set) noexcept {
  auto point_cloud = std::vector<std::array<double, 3>>{};
  point_cloud.reserve(set.size() * set.size() * set.size());

  for (const auto x : set)
    for (const auto y : set)
      for (const auto z : set)
        point_cloud.emplace_back(transformed_point(origin, translations.front(),
                                                   std::array{x, y, z}));
  return point_cloud;
}

template <>
std::vector<std::array<double, 2>> construct_periodic_point_cloud(
    const std::array<std::array<double, 2>, 2> &translations,
    std::array<double, 2> origin, const std::vector<double> &set) noexcept {
  auto point_cloud = std::vector<std::array<double, 2>>{};
  point_cloud.reserve(set.size() * set.size());

  for (const auto x : set)
    for (const auto y : set)
      point_cloud.emplace_back(
          transformed_point(origin, translations.front(), std::array{x, y}));
  return point_cloud;
}

template <>
std::vector<std::array<double, 1>> construct_periodic_point_cloud(
    const std::array<std::array<double, 1>, 1> &translations,
    std::array<double, 1> origin, const std::vector<double> &set) noexcept {
  auto point_cloud = std::vector<std::array<double, 1>>{};
  point_cloud.reserve(set.size());

  for (const auto x : set)
    point_cloud.emplace_back(
        transformed_point(origin, translations.front(), std::array{x}));
  return point_cloud;
}

} // namespace ae108::meshing
