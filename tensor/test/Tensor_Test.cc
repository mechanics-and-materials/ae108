// © 2020 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of ae108.
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/tensor/Tensor.h"
#include <array>
#include <gtest/gtest.h>

namespace ae108::tensor {
namespace {

TEST(Tensor_Test, zero_element_tensor_works) {
  static_assert(std::is_same_v<Tensor<double, 0>, std::array<double, 0>>,
                "A zero tensor is an array of length 0.");
}

TEST(Tensor_Test, one_element_tensor_works) {
  static_assert(std::is_same_v<Tensor<double, 1>, std::array<double, 1>>,
                "A zero tensor is an array of length 1.");
}

TEST(Tensor_Test, two_dimensional_tensor_works) {
  static_assert(std::is_same_v<Tensor<double, 1, 2>,
                               std::array<std::array<double, 2>, 1>>,
                "A two-dimensional tensor is an array of arrays.");
}

TEST(Tensor_Test, three_dimensional_tensor_works) {
  static_assert(
      std::is_same_v<Tensor<double, 1, 2, 3>,
                     std::array<std::array<std::array<double, 3>, 2>, 1>>,
      "A two-dimensional tensor is an array of arrays.");
}

} // namespace
} // namespace ae108::tensor
