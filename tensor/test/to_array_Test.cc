// © 2020 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of ae108.
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/tensor/Tensor.h"
#include "ae108/tensor/as_vector.h"
#include "ae108/tensor/to_array.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace ae108::tensor {
namespace {

TEST(to_array_Test, works_for_row_vector) {
  const auto v = Eigen::Matrix<int, 3, 1>(1, 2, 3);
  const auto arr = to_array(v);

  const auto expected = std::array<int, 3>{1, 2, 3};
  EXPECT_EQ(arr, expected);
}

TEST(to_array_Test, works_for_col_vector) {
  const auto v = Eigen::Matrix<int, 1, 3>(1, 2, 3);
  const auto arr = to_array(v);

  const auto expected = std::array<int, 3>{1, 2, 3};
  EXPECT_EQ(arr, expected);
}

TEST(to_array_Test, works_for_eigen_maps) {
  const auto original = std::array<int, 3>{1, 2, 3};
  const auto arr = to_array(as_vector(original));

  EXPECT_EQ(arr, original);
}

TEST(to_array_Test, works_for_eigen_expressions) {
  const auto a = std::array<int, 3>{1, 2, 3};
  const auto b = std::array<int, 3>{3, 2, 1};
  const auto c = to_array(as_vector(a) + as_vector(b));
  const auto expected_c = std::array<int, 3>{4, 4, 4};

  EXPECT_EQ(c, expected_c);
}

} // namespace
} // namespace ae108::tensor
