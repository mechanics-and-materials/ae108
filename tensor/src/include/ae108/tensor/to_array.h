// © 2020 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of ae108.
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "ae108/tensor/as_vector.h"
#include <Eigen/Core>

namespace ae108::tensor {

/**
 * @brief Converts an Eigen column or row vector to a std::array.
 */
template <class EigenType_>
std::array<typename EigenType_::Scalar,
           EigenType_::ColsAtCompileTime * EigenType_::RowsAtCompileTime>
to_array(const EigenType_ &vec) {
  static_assert(EigenType_::RowsAtCompileTime != -1 &&
                    EigenType_::ColsAtCompileTime != -1,
                "Dynamically sized Eigen matrices are not supported");
  static_assert(EigenType_::ColsAtCompileTime == 1 ||
                    EigenType_::RowsAtCompileTime == 1,
                "Only Eigen row / column vectors (matrices of shape (1, *) / "
                "(*, 1)) are supported");
  auto arr = std::array<typename EigenType_::Scalar,
                        EigenType_::ColsAtCompileTime *
                            EigenType_::RowsAtCompileTime>{};
  as_vector(arr) = vec;
  return arr;
}

} // namespace ae108::tensor
