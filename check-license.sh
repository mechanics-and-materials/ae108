#!/bin/bash
# © 2024 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of qcmesh.
#
# qcmesh is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# qcmesh is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# qcmesh. If not, see <https://www.gnu.org/licenses/>.

files=$(grep \
        --recursive \
        --exclude-dir='.git' \
        --exclude-dir='build*' \
        --exclude-dir='.mypy_cache' \
        --exclude-dir='__pycache__' \
        --exclude='LICENSE' \
        --exclude='*.json' \
        --exclude='*.md' \
        --exclude='*.png' \
        --exclude='*.ico' \
        --exclude='*.gif' \
        --exclude='*.svg' \
        --exclude='*.vtu' \
        --exclude='*.txt' \
        "$@" \
        --files-without-match ' © [0-9, -]* ETH Zurich, Mechanics and Materials Lab' \
        .
      )
test -n "$files" && echo "$files" >&2 && exit 1
exit 0
