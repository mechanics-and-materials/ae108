# © 2020 ETH Zurich, Mechanics and Materials Lab
# © 2020 California Institute of Technology
#
# This file is part of ae108.
#
# ae108 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any
# later version.
#
# ae108 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ae108. If not, see <https://www.gnu.org/licenses/>.

---
site_name: "ae108"
site_url: https://ae108.ethz.ch
site_author: ETH Zurich, Mechanics and Materials Lab
site_description: >-
  Scalable variational framework for FEA.
repo_url: https://gitlab.ethz.ch/mechanics-and-materials/ae108
copyright: Copyright © 2024 ETH Zurich, Mechanics and Materials Lab

docs_dir: content
site_dir: public

markdown_extensions:
  - admonition
  - attr_list
  - md_in_html
  - pymdownx.details
  - pymdownx.superfences
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.snippets
  - pymdownx.highlight:
      pygments_lang_class: true
      use_pygments: true
extra:
  generator: false

theme:
  name: material
  language: en
  logo: assets/logo_dark.svg
  favicon: assets/favicon.ico
  palette:
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      primary: brown
      accent: brown
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      primary: brown
      accent: brown
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode

plugins:
  - search
  - mkdoxy:
      projects:
        ae108:
          src-dirs: "assembly cmdline cpppetsc cppptest cppslepc elements meshing solve tensor"
          full-doc: true
          doxy-cfg:
            EXCLUDE_PATTERNS: "*/test/*"
            RECURSIVE: true
            FILE_PATTERNS: "*.h,*.dox"
            EXTRACT_ALL: false
            EXCLUDE_SYMBOLS: "detail::*,*::detail::*,*::detail,boost,boost::*,std,mpl"

nav:
  - "Home": "index.md"
  - "Quickstart": "quickstart.md"
  - "Examples":
      - "Info": "examples/index.md"
      - "cmdline": "examples/cmdline.md"
  - "FAQ": "faq.md"
  - API Reference: ae108/namespaceae108.md
