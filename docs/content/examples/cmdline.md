# cmdline tutorial

To use this library, you only need to familiarize yourself with a single class.
This class is called `CommandLineOptionParser`.
It's in the namespace `ae108::cmdline`.

## Example Application

Let's try using this class in a simple example.
In this example, we are going greet the world if greetings are enabled via a command line flag.

First, we include the necessary headers and start with defining a `main` function.

```cpp title="examples/Cmdline.cc:18"
#include <ae108/cmdline/CommandLineOptionParser.h>
#include <iostream>

int main(const int argc, const char *const *const argv) {
  namespace cmdline = ae108::cmdline;
```

To parse the command line parameters, we'll construct a `CommandLineOptionParser` by providing a stream.
Let's say we want to print error messages and warnings to `stderr`.
In addition, we define a boolean variable `enable_greeting` that will store whether the greeting was enabled.

```cpp title="examples/Cmdline.cc:24"
  auto enable_greeting = false;
  cmdline::CommandLineOptionParser(std::cerr)
```
We'll add a flag `--enable_greeting` (with a short form `-g`) together with a help text describing the flag.
The `CommandLineOptionParser` class provides a `withOption` method that we are going to use to achieve that.

```cpp title="examples/Cmdline.cc:26"
      .withOption("enable_greeting,g", "Print a greeting.", &enable_greeting)
```
Now that we've configured the `CommandLineOptionParser`, the only thing that is missing is using it to parse the command line options in `argc`/`argv`.

```cpp title="examples/Cmdline.cc:27"
      .parse(argc, argv);
```

Finally we print a message if greetings have been enabled.

```cpp title="examples/Cmdline.cc:29"
  if (enable_greeting) {
    std::cout << "Hello world!" << '\n';
  }
```

Let's see the application in action.

```sh
$ cmdline/examples/ae108-CmdLineExample
$ cmdline/examples/ae108-CmdLineExample --enable_greeting=yes
Hello world!
$ cmdline/examples/ae108-CmdLineExample --enable_greeting=no
$ cmdline/examples/ae108-CmdLineExample -g yes
Hello world!
```

As promised, there's also a `--help` flag:

```sh
$ cmdline/examples/ae108-CmdLineExample --help
Command line options:
-h [ --help ]                Show this help.
-g [ --enable_greeting ] arg Print a greeting.
```

Moreover, a warning message is printed out to `stderr` if an unknown flag is used.

```sh
$ cmdline/examples/ae108-CmdLineExample --unknown_flag=123
Warning: The following options were not recognized by cmdline: '--unknown_flag=123'.
```

Most importantly, invalid command line parameters are rejected with a suitable error message.

```sh
$ cmdline/examples/ae108-CmdLineExample --enable_greeting=123
the argument ('123') for option '--enable_greeting' is invalid. Valid choices are 'on|off', 'yes|no', '1|0' and 'true|false'
```


## Using the Example

The full source code of the example is available in [examples/Cmdline.cc](https://gitlab.ethz.ch/mechanics-and-materials/ae108/-/blob/main/examples/Cmdline.cc).

If you want to build it and try it out, then compile the executable target `ae108-examples-Cmdline` and run it with command line options of your choice.

## Outlook

We've seen many of the features in action, but there's a bit more to explore.
For instance, it's possible to chain more than one call to `withOption`:

```cpp
cmdline::CommandLineOptionParser(std::cerr)
    .withOption(/* ... */)
    .withOption(/* ... */)
    .parse(argc, argv);
```

Also, there is another overload of `withOption` that permits to add flags without a help text.

You can find additional information in the [API documentation](../ae108/namespaceae108_1_1cmdline.md) of the library.

In addition, the tests in [cmdline/test/CommandLineOptionParser_Test.cc](https://gitlab.ethz.ch/mechanics-and-materials/ae108/-/blob/main/cmdline/test/CommandLineOptionParser_Test.cc) showcase the features in common use cases.
