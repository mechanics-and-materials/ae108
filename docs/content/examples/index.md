# Examples

*ae108* is the basis of many research projects in the [Mechanics & Materials Lab at ETH Zurich](https://mm.ethz.ch/). 
A complete and updated list of research examples can be found [in our list of publications](https://mm.ethz.ch/publications-and-awards/journal-articles.html).
Some of the fundamental functionalities are highlighted in the [tutorial examples](https://gitlab.ethz.ch/mechanics-and-materials/ae108/-/tree/main/examples).
