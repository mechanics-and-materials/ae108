﻿# ae108

*a scalable variational framework for FEA*

[![source: ETHZ GitLab](https://img.shields.io/badge/source-ETHZ-red?logo=gitlab)](https://gitlab.ethz.ch/mechanics-and-materials/ae108/)
[![license: GPLv3](https://img.shields.io/badge/license-GPLv3-blue)](https://www.gnu.org/licenses/gpl-3.0)

*ae108* is a scalable (parallel) C++ framework for computational solid mechanics simulations using a variational approach, primarily focusing on the Finite Element Analysis (FEA).
See the [Computational Solid Mechanics lecture notes]( https://ethz.ch/content/dam/ethz/special-interest/mavt/mechanical-systems/mm-dam/documents/Notes/CompMech_Notes.pdf) for more information about the approach that is used.
The code is developed by the [Mechanics & Materials Lab at ETH Zurich](https://mm.ethz.ch/).

!!! note

    *ae108* is a research library. The libary, as well as this documentation,
    is under active development. The contents are constantly expanding and may
    be subject to frequent changes. For the latest information, please refer to
    our [GitLab](https://gitlab.ethz.ch/mechanics-and-materials/ae108).

For general citations on *ae108* please use the following:

```bib
@misc{ae108,
    author = {Mechanics and Materials Lab},
    copyright = {GNU General Public License v3.0 or later (GPL-3.0-or-later)},
    title = {ae108},
    doi = {https://doi.org/10.5905/ethz-1007-257},
    year = {2020},
}
```

<div class="grid cards" markdown>

- ![](img/trussIndentation.gif)
- ![](img/crystalPlasticity.png)
- ![](img/wavePropagation.gif)
- ![](img/homogenization.png)
- ![](img/neoHookeanBall.gif)

</div>
