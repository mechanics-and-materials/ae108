# Frequently Asked Questions

## Setup

!!! question

    How do I limit the number of cores that are used to build the project
    in VS Code?


!!! info "Answer"

    The [CMake Tools extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools)
    supports a [number of parameters to configure the build](https://github.com/microsoft/vscode-cmake-tools/blob/main/docs/cmake-settings.md).
    Those can be set in the file `.vscode/settings.json`. To limit the
    number of cores use the parameter `cmake.parallelJobs`.

!!! question
 
    How do I configure SSH on Windows?
 
!!! info "Answer"
 
    The SSH configuration file on Windows 10 is located at
    `C:\Users\<username>\.ssh\config`.
    Note that the contents of this file also have an impact on how
    Docker connects to a remote container. For instance, the file
    could look like this (replace `(...)` by suitable settings);
    see also the
    [Euler documentation](https://scicomp.ethz.ch/wiki/Getting_started_with_clusters#SSH):
 
    ```
    Host (...)
        User (...)
        HostName (...)
        IdentityFile (...)
    ```
 
    In case VS Code asks for the key passphrase too often, consider
    setting up the SSH agent; see the [VS Code documentation](https://code.visualstudio.com/docs/remote/troubleshooting#_setting-up-the-ssh-agent)
    for more information.
 
!!! question
 
    I cannot attach to containers via SSH with VS Code.
 
!!! info "Answer"
 
    First check that attaching to the workstation via VS Code and SSH
    works, and that the containers can be started via the
    `docker-compose.yml` file in the repository. If this is the case,
    make sure that the rootless Docker context is activated on the
    workstation. For this purpose run `docker context use rootless` on
    the workstation. If the problem persists, try uninstalling and
    installing rootless Docker on the workstation (following the
    oinstructions
    [here](https://docs.docker.com/engine/security/rootless/)), and
    then activating the rootless context.
 
!!! question
 
    VS Code reports a “Bad CMake executable”.
 
!!! info "Answer"
 
    If VS Code shows the error
    
    ```
    Bad CMake executable "". Is it installed or settings contain the correct path (cmake.cmakePath)?
    ```
    
    then the most likely reason for the error is that the VS Code windows
    is not yet attached to a container. If you are using a custom
    container, then make sure that CMake is installed in the container.
 
!!! question
 
    VS Code asks for a password even though I have created an SSH key.
 
!!! info "Answer"
 
    Please make sure that you have provided the public key in the
    workstation’s `authorized_keys` file; see e.g.
    [scicomp.ethz.ch](https://scicomp.ethz.ch/wiki/Getting_started_with_clusters#SSH_keys).
 
!!! question
 
    When interacting with `git` I am asked for a password even though
    I’ve added a public key.
 
!!! info "Answer"
 
    `git` may not be aware of the key to use for the remote repository.
    To solve the problem, add an entry to `.ssh/config` (or similar),
    specifying the private key to use (replace `(...)` by a suitable
    path):
 
    ```
    Host gitlab.ethz.ch
        HostName gitlab.ethz.ch
        IdentityFile (...)
    ```
 
!!! question
 
    Attaching to the container with VS Code fails with the error message
    
    ```
    Cannot connect to the Docker daemon at unix:///run/user//docker.sock. Is the docker daemon running?
    ```
    
    (note the missing user ID between `/user/` and `/docker.sock`).
 
!!! info "Answer"
 
    This error was caused by an incompatibility of the
    [Remote-Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
    plugin with the Leibniz configuration. The error should no longer
    occur with the latest version of the plugin.
 
!!! question
 
    How can I link against a version of PETSc that is compiled with
    enabled debug checks?
 
!!! info "Answer"
 
    Install the Ubuntu package `libpetsc-real3.12-dbg` (if you are
    using a real scalar type). Then, before running `CMake`, set the
    environment variable `PKG_CONFIG_PATH` to
    `/usr/lib/petscdir/petsc3.12/x86_64-linux-gnu-real-debug/lib/pkgconfig`
    (remember to clear `CMake`'s cache if it does exist already). If
    you are using VS Code, this can also be accomplished by adding the
    following to `settings.json`:
 
    ```json
    "cmake.configureEnvironment": {
      "PKG_CONFIG_PATH": "/usr/lib/petscdir/petsc3.12/x86_64-linux-gnu-real-debug/lib/pkgconfig"
    }
    ```
 
!!! question
 
    Why do I see the error `Read -1, expected ..., errno =1` when
    running an executable in parallel in the Docker container?
 
!!! info "Answer"
 
    This is a known issue, see the 
    [corresponding issue on GitHub](https://github.com/open-mpi/ompi/issues/4948).
    Setting the following environment variable works:
 
    ```sh
    export OMPI_MCA_btl_vader_single_copy_mechanism=none
    ```
 
!!! question
 
    How can I add a new developer user to the project?
 
!!! info "Answer"
 
    Please ask the new developer to login to `gitlab.ethz.ch` once.
    Then send an email to an ae108 maintainer with the
    username of the new developer.
 
!!! question
 
    The solver does not converge for my scenario. What can I do?
 
!!! info "Answer"
 
    [Jed Brown](https://www.colorado.edu/cs/jed-brown) (one of the
    PETSc developers) offers advice for both [nonlinear systems](https://scicomp.stackexchange.com/questions/30/why-is-newtons-method-not-converging)
    and [linear systems](https://scicomp.stackexchange.com/questions/513/why-is-my-iterative-linear-solver-not-converging).
 
!!! question
 
    How can I use distributed SuperLU with PETSc?
 
!!! info "Answer"
 
    According to the [PETSc documentation](https://petsc.org/release/docs/manualpages/Mat/MATSOLVERSUPERLU_DIST.html#MATSOLVERSUPERLU_DIST),
    this is possible with the flags
    `-pc_type lu -pc_factor_mat_solver_type superlu_dist`.
 
!!! question
 
    When I submit my job on Euler via `sbatch, the error message is
    “Permission denied”. How can I fix this?
 
!!! info "Answer"
 
    Very often, the reason for this error is that the file that `sbatch`
    tries to execute is not executable (e.g. a text file). In this case
    Linux refuses to execute the file. You can confirm that this is the
    issue by attempting to execute the file on the login node
    (i.e. without `sbatch`); this should yield the same error.
    Frequently, the problem is that a different executable was intended
    to be used. Otherwise it is possible to mark a file as executable via
    `chmod +x` followed by the path to the file.
 
!!! question
 
    Docker Desktop fails to start on Windows.
 
!!! info "Answer"
 
    To use Docker Desktop on Windows, make sure to install
    [Ubuntu 22.04 from the Microsoft store](https://apps.microsoft.com/store/detail/ubuntu-2204-lts/9PN20MSR04DW),
    and activate “Use the WSL 2 based engine” in the Docker settings
    (“General”).

## Elements

!!! question
 
    What do I need to do to write a material model?
 
!!! info "Answer"
 
    Create a header file in `src/include/ae108/elements/materialmodels/` and
    an implementation file (that includes the header file) in
    `src/materialmodels/` and add the latter to the `CMakeLists.txt` in the
    `src` directory.
    If you don’t want your material model to be part of this library you can
    also put those files at a different location.

    Now write a class that contains all the data and implement the traits that
    you want to support.
    Note that the new material model should derive from `MaterialModelBase`.
    Feel free to look at the `Hookean` material model for inspiration.

    To test your material model add an implementation file to the 
    `test/materialmodels` directory and add it to the `CMakeLists.txt`
    in the `test` directory.
    Make sure you instantiate the `MaterialModel_Test` set of tests for your
    class to run many checks that apply to all material models (e.g. consistent
    derivatives) automatically.
    Again, have a look at `Hookean_Test` to see an example of how this can be
    done.

!!! question

    Why are there traits (like `ComputeEnergyTrait`) and free functions (like
    `compute_energy`)?

!!! info "Answer"

    Traits make it very easy to choose an implementation of “computing energy”
    without copying and pasting code.
    This provides a lot of flexibility when writing elements.
    However, this flexibility may make it harder to use an element because 
    every specialization of an `ComputeEnergyTrait` could e.g. return a different type.
    So, the goal is to only depend on properties of `ElementBase` so that 
    calling code can adjust easily.

    To solve this problem the free function `compute_energy` promises an 
    interface to users: it always returns `ElementBase::Energy` which is a name 
    for the element’s `value_type`.
    It also simplifies using the trait.

    To sum up:

    - Authors of elements should implement traits that are compatible with the free functions.
    - Users of elements should use the free functions to interact with elements.

    The same reasoning applies to other components like material models, etc.

!!! question

    Why are the `operator()` methods of many traits template functions?

!!! info "Answer"

    The idea behind using traits is to make it possible to reuse implemented 
    traits for different classes.
    If an implementer of e.g. a material model `B` decides to reuse the 
    implementation of the `ComputeEnergyTrait` of class `A`, the following code 
    should be sufficient:

    ```cpp
    template<>
    struct ComputeEnergyTrait<B> : ComputeEnergyTrait<A> {};
    ```

    Now assume that you have an instance `b` of a `B` and that `operator()` is 
    not a template.
    When calling the free function `compute_energy(b, ...)` compilation would 
    fail because `operator()` of `B`'s `ComputeEnergyTrait` needs to be called 
    with an instance of `A` (since we are inheriting this operator from `A`'s $
    trait).

    By declaring `operator()` a template function this use case works.
