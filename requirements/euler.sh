# © 2022 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of ae108.
#
# ae108 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any
# later version.
#
# ae108 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ae108. If not, see <https://www.gnu.org/licenses/>.

# Required modules/packages for building/testing the project on Euler (https://scicomp.ethz.ch/wiki/Euler)

module load stack/2024-06
# Enable M&M overlay:
module use "/cluster/project/mandm/cluster_modules/lmod/linux-ubuntu22.04-x86_64/Core"

module load \
       openmpi/4.1.6 \
       cmake \
       ninja \
       eigen \
       boost \
       hdf5 \
       voropp

# modules from M&M overlay:
module load \
       googletest-src \
       petsc/3.15.5 \
       slepc/3.15.2

module load python
python -m pip install vtk
