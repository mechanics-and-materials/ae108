// © 2020, 2021 ETH Zurich, Mechanics and Materials Lab
//
// This file is part of ae108.
//
// ae108 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or any
// later version.
//
// ae108 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with ae108. If not, see <https://www.gnu.org/licenses/>.

#include "ae108/assembly/Assembler.h"
#include "ae108/assembly/FeaturePlugins.h"
#include "ae108/assembly/plugins/AssembleEnergyPlugin.h"
#include "ae108/assembly/plugins/AssembleForceVectorPlugin.h"
#include "ae108/assembly/plugins/AssembleMassMatrixPlugin.h"
#include "ae108/assembly/plugins/AssembleStiffnessMatrixPlugin.h"
#include "ae108/cpppetsc/Mesh.h"
#include "ae108/cpppetsc/ParallelComputePolicy.h"
#include "ae108/cpppetsc/Vector.h"
#include "ae108/cpppetsc/computeElementsOfMatrix.h"
#include "ae108/cppslepc/Context.h"
#include "ae108/cppslepc/computeSmallestEigenvalues.h"
#include "ae108/elements/ElementWithMass.h"
#include "ae108/elements/TimoshenkoBeamElement.h"
#include "ae108/elements/compute_mass_matrix.h"
#include "ae108/meshing/refine_segment_mesh.h"
#include "ae108/tensor/as_vector.h"
#include "ae108/tensor/to_array.h"
#include <array>
#include <cmath>
#include <cstddef>
#include <petscsys.h>

namespace cpppetsc = ae108::cpppetsc;
namespace cppslepc = ae108::cppslepc;
namespace elements = ae108::elements;
namespace assembly = ae108::assembly;

using Policy = cpppetsc::ParallelComputePolicy;
using Context = cppslepc::Context<Policy>;
using Mesh = cpppetsc::Mesh<Policy>;
using Vector = cpppetsc::Vector<Policy>;

// In this example we will calculate the eigenfrequencies of an unsupported
// Euler-Bernoulli beam and compare to the analytical solution.
// https://en.wikipedia.org/wiki/Euler-Bernoulli_beam_theory#Example:_unsupported_(free-free)_beam

// A beam with length 20 is discretized into 10 elements
//
// 0--2--3--4--5--6--7--8--9--10--1

constexpr auto coordinate_dimension = Mesh::size_type{2};

using Point = std::array<Mesh::real_type, coordinate_dimension>;

using TimoshenkoBeamElement =
    elements::TimoshenkoBeamElement<coordinate_dimension, Vector::value_type,
                                    Vector::real_type>;
using Element = elements::ElementWithMass<TimoshenkoBeamElement>;

using Properties =
    elements::TimoshenkoBeamProperties<Mesh::real_type, coordinate_dimension>;

// Let's not forget about the AssembleMassMatrixPlugin as we will need it to
// form the eigenvalue problem
using Plugins =
    assembly::FeaturePlugins<assembly::plugins::AssembleEnergyPlugin,
                             assembly::plugins::AssembleForceVectorPlugin,
                             assembly::plugins::AssembleStiffnessMatrixPlugin,
                             assembly::plugins::AssembleMassMatrixPlugin>;

using Assembler = assembly::Assembler<Element, Plugins, Policy>;

// We assume Euler-Bernoulli beam theory and hence chose the shear correction
// factor to be zero, such that the Timoshenko beam description reduces to the
// Euler-Bernoulli beam theory
constexpr Mesh::real_type young_modulus = 1.;
constexpr Mesh::real_type poisson_ratio = 0.3;
constexpr Mesh::real_type shear_modulus =
    young_modulus / (2 * (1 + poisson_ratio));
constexpr Mesh::real_type shear_correction_factor_y = 0.;
constexpr Mesh::real_type beam_length = 20.;
constexpr Mesh::real_type width = 1.;
constexpr Mesh::real_type thickness = 1.;
constexpr Mesh::real_type area = width * thickness;
constexpr Mesh::real_type density = 1.;
constexpr Mesh::real_type area_moment_z =
    thickness * width * width * width / 12.;

// It can be shown that the natural frequencies of an unsupported (free-free)
// Euler-Bernoulli beam are as follows:
const auto analytic_result = [](std::size_t n) {
  return std::pow(static_cast<double>(2 * n + 1) * M_PI / 2. / beam_length, 2) *
         std::sqrt(young_modulus * area_moment_z / (density * area));
};

using ae108::tensor::as_vector;

int main(int argc, char **argv) {

  // MPI/PETSc/cpppetsc must be initialized before using it.
  const auto context = Context(&argc, &argv);

  // The beam with defined length is discretized into 10 segments
  const auto geometry = ae108::meshing::refine_segment_mesh(
      ae108::meshing::MeshGeometry<Point, std::array<std::size_t, 2>>{
          {{0., 0.}, {beam_length, 0.}},
          {{0, 1}},
      },
      beam_length / 10);

  // Let's create the mesh and an assembler.
  const auto mesh = Mesh::fromConnectivity(
      coordinate_dimension, geometry.connectivity,
      static_cast<Mesh::size_type>(geometry.positions.size()),
      Element::degrees_of_freedom(), 0);

  auto assembler = Assembler();

  Properties const properties = {
      young_modulus, shear_modulus, shear_correction_factor_y,
      area,          area_moment_z,
  };

  for (const auto &element : mesh.localElements()) {
    const auto [a, b] = geometry.connectivity[element.index()];
    auto element_axis = ae108::tensor::to_array(
        as_vector(geometry.positions[a]) - as_vector(geometry.positions[b]));

    assembler.emplaceElement(
        element, Element{{},
                         // .element =
                         Element::Element{{},
                                          // .stiffness_matrix =
                                          timoshenko_beam_stiffness_matrix(
                                              element_axis, properties)},
                         // .mass_matrix =
                         timoshenko_beam_consistent_mass_matrix(
                             element_axis, properties, density)});
  }

  // Let's assemble the stiffness and mass matrices.
  const auto K = [&]() {
    auto K = Mesh::matrix_type::fromMesh(mesh);
    assembler.assembleStiffnessMatrix(Mesh::vector_type::fromLocalMesh(mesh),
                                      Element::Time{0.}, &K);
    K.finalize();
    return K;
  }();

  const auto M = [&]() {
    auto M = Mesh::matrix_type::fromMesh(mesh);
    assembler.assembleMassMatrix(&M);
    M.finalize();
    return M;
  }();

  // Finally, we can compute the eigenvalues.
  const auto number_of_eigenvalues = Mesh::size_type(10);
  const auto result = cppslepc::computeSmallestEigenvalues(
      cpppetsc::computeElementsOfMatrix(K),
      cpppetsc::computeElementsOfMatrix(M), number_of_eigenvalues);

  // Let's compare the results to the analytical solution.
  for (std::size_t n = 0; n < 4; n++) {
    const auto &value = result[n + 3];
    Policy::handleError(
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg,hicpp-vararg)
        PetscPrintf(Policy::communicator(),
                    "natural frequency %zu: %.4f (error: %3.2f%%)\n", n,
                    std::sqrt(value).real(),
                    (std::sqrt(value).real() - analytic_result(n)) /
                        analytic_result(n) * 100));
  }
}
