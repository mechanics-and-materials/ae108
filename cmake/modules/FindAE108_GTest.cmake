# © 2020 ETH Zurich, Mechanics and Materials Lab
# © 2020 California Institute of Technology
#
# This file is part of ae108.
#
# ae108 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any
# later version.
#
# ae108 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ae108. If not, see <https://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.10 FATAL_ERROR)

if (NOT TARGET GTest::gtest)
  find_path(GOOGLETEST_SRC_DIRECTORY
    "googletest/CMakeLists.txt"
    NO_DEFAULT_PATH
    HINTS "/usr/src/googletest" $ENV{GTEST_SRC} ${GTEST_SRC}
  )

  if(EXISTS "${GOOGLETEST_SRC_DIRECTORY}")
    add_subdirectory("${GOOGLETEST_SRC_DIRECTORY}" "${CMAKE_CURRENT_BINARY_DIR}/3rdParty/gtest" EXCLUDE_FROM_ALL)
  else()
    find_package(GTest 1.8.1 CONFIG REQUIRED)
  endif()
endif()
