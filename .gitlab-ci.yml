# © 2020 ETH Zurich, Mechanics and Materials Lab
# © 2020 California Institute of Technology
#
# This file is part of ae108.
#
# ae108 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any
# later version.
#
# ae108 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ae108. If not, see <https://www.gnu.org/licenses/>.

---
stages:
  - build-image
  - analyze
  - build
  - test-install
  - publish

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH

.build-image: &build-image
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - /kaniko/executor
        --dockerfile docker/$DOCKERFILE_NAME
        --build-arg PETSC_SCALAR_TYPE=${PETSC_SCALAR_TYPE:-real}
        --build-arg BASE_IMAGE=${BASE_IMAGE:-ubuntu:jammy}
        --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_REF_SLUG
        --cache-ttl=730h
        --cache=true

build-image-dev-real:
  stage: build-image
  variables:
    DOCKERFILE_NAME: "Dockerfile"
    PETSC_SCALAR_TYPE: "real"
    IMAGE_NAME: "dev-real"
  <<: *build-image

build-image-dev-complex:
  stage: build-image
  variables:
    DOCKERFILE_NAME: "Dockerfile"
    PETSC_SCALAR_TYPE: "complex"
    IMAGE_NAME: "dev-complex"
  <<: *build-image

build-image-doc:
  stage: build-image
  variables:
    DOCKERFILE_NAME: "doc/Dockerfile"
    IMAGE_NAME: "doc"
    BASE_IMAGE: "debian:bookworm"
  <<: *build-image

build-image-lint:
  stage: build-image
  variables:
    DOCKERFILE_NAME: "Dockerfile"
    IMAGE_NAME: "lint"
    BASE_IMAGE: "ubuntu:noble"
  <<: *build-image

check-code-format:
  stage: analyze
  needs: ["build-image-dev-real"]
  image: $CI_REGISTRY_IMAGE/dev-real:$CI_COMMIT_REF_SLUG
  script:
    - >
      find . \
        -name "*.h" -print0 \
        -or \
        -name "*.cc" -print0 \
      | xargs -0 -I {} -P $(nproc) \
        clang-format --dry-run --Werror {} \
      || (echo -e 'Run the following command to fix format:\nfind . -name "*.h" -print0 -or -name "*.cc" -print0 | xargs -0 -I {} clang-format -i {}' \
      && exit 1)

lint:
  stage: analyze
  needs: ["build-image-lint"]
  image:
    name: $CI_REGISTRY_IMAGE/lint:$CI_COMMIT_REF_SLUG
  script:
    - CMAKE_EXPORT_COMPILE_COMMANDS=1 cmake -B build -S . -G Ninja -DCMAKE_BUILD_TYPE=Release
    - run-clang-tidy -j 4 -quiet -p build/

check-license-header:
  needs: []
  stage: analyze
  image: debian:stable-slim
  script: ./check-license.sh

lint-script:
  stage: analyze
  needs: []
  image: python:3-slim
  before_script:
    - pip install mypy ruff
  script:
    - mypy --ignore-missing --exclude build-doc/source/conf.py .
    - ruff check --config tools/ruff.toml
    - ruff format --check --config tools/ruff.toml

check-script:
  stage: analyze
  needs: ["build-image-dev-real"]
  image: $CI_REGISTRY_IMAGE/dev-real:$CI_COMMIT_REF_SLUG
  script:
    - find . -name "*.py" -print0 | xargs -0 python3 -m doctest

.build-library: &build-library
  stage: build
  coverage: '/^lines:\s*\d+\.\d+\%/'
  script:
    - useradd developer
    - mkdir build
    - chown developer:developer build
    - >
      su developer -c '
        cmake -B build -S . -G Ninja -DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="--coverage -Werror -Wall -Wextra -Wpedantic" &&
        cmake --build build &&
        cd build &&
        GTEST_OUTPUT="xml:gtest-results.xml" ctest --output-on-failure --exclude-regex ".*_mpi$" &&
        ctest --output-on-failure --tests-regex ".*_mpi$" &&
        python3 -m gcovr --print-summary --xml coverage.xml -j$(nproc) --root .. .
      '
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: build/coverage.xml
      junit: build/*/test/gtest-results.xml
    when: always

build-library-real:
  image: $CI_REGISTRY_IMAGE/dev-real:$CI_COMMIT_REF_SLUG
  needs: ["build-image-dev-real"]
  <<: *build-library

build-library-complex:
  image: $CI_REGISTRY_IMAGE/dev-complex:$CI_COMMIT_REF_SLUG
  needs: ["build-image-dev-complex"]
  <<: *build-library

.install-library: &install-library
  stage: build
  script:
    - mkdir build
    - >
      bash -c '
        cmake -B build -S . -G Ninja -DCMAKE_BUILD_TYPE=Debug &&
        cmake --build build --target install
      '
    - mkdir build-examples
    - >
      bash -c '
        cmake -B build-examples -S examples -G Ninja -DCMAKE_BUILD_TYPE=Debug &&
        cmake --build build-examples
      '

install-library-real:
  image: $CI_REGISTRY_IMAGE/dev-real:$CI_COMMIT_REF_SLUG
  needs: ["build-image-dev-real"]
  <<: *install-library

install-library-complex:
  image: $CI_REGISTRY_IMAGE/dev-complex:$CI_COMMIT_REF_SLUG
  needs: ["build-image-dev-complex"]
  <<: *install-library

.build-deb: &build-deb
  stage: build
  script:
    - useradd developer
    - mkdir build
    - chown developer:developer build
    - >
      su developer -c '
        cmake -B deb -S . -G Ninja \
          -DCMAKE_BUILD_TYPE=Release \
          -DCMAKE_CXX_FLAGS="-Werror -Wall -Wextra -Wpedantic" \
        && cmake --build deb \
        && cd deb && cpack
      '
  artifacts:
    paths:
      - deb/*.deb
  rules:
    - if: '$CI_COMMIT_REF_PROTECTED'

build-deb-real:
  image: $CI_REGISTRY_IMAGE/dev-real:$CI_COMMIT_REF_SLUG
  needs: ["build-image-dev-real"]
  <<: *build-deb

build-deb-complex:
  image: $CI_REGISTRY_IMAGE/dev-complex:$CI_COMMIT_REF_SLUG
  needs: ["build-image-dev-complex"]
  <<: *build-deb

.install-deb: &install-deb
  stage: test-install
  image: ubuntu:jammy
  script:
    - apt-get update && apt-get install -y cmake ninja-build
    - >
      DEBIAN_FRONTEND="noninteractive" TZ="Europe/Zurich"
      apt-get -f install -y ./deb/libae108-${SCALAR_TYPE}-dev-*.deb
    - cmake -B build -S examples -G Ninja -DCMAKE_BUILD_TYPE=Debug
    - cmake --build build
  rules:
    - if: '$CI_COMMIT_REF_PROTECTED'

install-deb-real:
  needs: ["build-deb-real"]
  variables:
    SCALAR_TYPE: real
  <<: *install-deb

install-deb-complex:
  needs: ["build-deb-complex"]
  variables:
    SCALAR_TYPE: complex
  <<: *install-deb

package:
  stage: publish
  image: curlimages/curl:latest
  script:
    - >
      for p in deb/*.deb ; do
       stem="${p%-*}" ; \
       version="${stem##*-}" ; \
       version_ext="${version}+${CI_COMMIT_SHORT_SHA}"
       curl \
        --header "JOB-TOKEN: $CI_JOB_TOKEN" \
        --upload-file "$p" \
        "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/deb/${CI_COMMIT_TAG:-$version_ext}/$(basename $p)" ;
      done
  rules:
    - if: '$CI_COMMIT_REF_PROTECTED'


build-documentation:
  stage: build
  needs: ["build-image-doc"]
  image: $CI_REGISTRY_IMAGE/doc:$CI_COMMIT_REF_SLUG
  script:
    - python3 docs/check-codeblocks.py
        --src-attr=title
        --doc docs/content
        --src .
    - mkdocs build
        --config-file docs/mkdocs.yml
        --site-dir ../public
  artifacts:
    paths:
      - public

build-euler-modules:
  stage: build
  needs: []
  script:
    - . requirements/euler.sh
    - >
      cmake -B build-release -S . -G Ninja -DCMAKE_BUILD_TYPE=Release
    - >
      srun -c 32 --mem-per-cpu=4G --account=es_kochm --time=01:00:00 cmake --build build-release --target all
    # srun does not work with mpi for some reason - sbatch works:
    - >
      if ! sbatch -o euler-ae108-test.log -e euler-ae108-test.log -W --ntasks=3 --account=es_kochm --time=01:00:00 --wrap "cmake --build build-release --target test" ; then
      exit_code=1 ;
      else
      exit_code=0 ;
      fi
    - cat euler-ae108-test.log
    - exit "$exit_code"
  tags:
    - euler
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: '$CI_COMMIT_BRANCH != "main"'
      when: manual
      allow_failure: true

publish-html:
  stage: publish
  image: alpine
  before_script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - echo "$PUBLISH_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>/dev/null
    - host="${PUBLISH_URL%:*}"
    - host="${host#*@}"
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan "$host" 2>/dev/null >> ~/.ssh/known_hosts
    - >
      [ -n "$CI_COMMIT_BRANCH" ] \
        && [ "$CI_COMMIT_BRANCH" != "$CI_DEFAULT_BRANCH" ] \
        && mv public "${CI_COMMIT_BRANCH}" \
        && mkdir public && mv "${CI_COMMIT_BRANCH}" public/
  script:
    - |-
      sftp -q -b - "${PUBLISH_URL%:*}" <<EOF
      put -r public/* ${PUBLISH_URL#*:}
      EOF
  rules:
    - if: '$CI_COMMIT_REF_PROTECTED && $PUBLISH_URL && $PUBLISH_KEY'
