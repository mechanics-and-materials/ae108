#!/usr/bin/env python3

# © 2021 ETH Zurich, Mechanics and Materials Lab
#
# This file is part of ae108.
#
# ae108 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or any
# later version.
#
# ae108 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ae108. If not, see <https://www.gnu.org/licenses/>.

"""Runs the tests defined in definition.json files."""

import json
import math
import os
import pathlib
import re
import shutil
import subprocess
import sys
import tempfile
import unittest
from collections.abc import Callable, Iterable
from contextlib import contextmanager
from dataclasses import dataclass, field
from difflib import unified_diff
from typing import Any

ROOT_DIRECTORY = pathlib.Path(__file__).resolve().parent.parent


@dataclass(frozen=True)
class BinaryOutputDefinition:
    """Contains the parameters necessary to compare binary output."""

    filename: pathlib.Path
    xdmf_generator_flags: list[str] = field(default_factory=list)

    @classmethod
    def from_dict(cls, data: dict[str, Any]) -> "BinaryOutputDefinition":
        return cls(filename=pathlib.Path(data.pop("filename")), **data)


def no_diff(_a: str, _b: str, _name: str) -> None:
    """Diff callback with always succeeds (does not compare)."""


@dataclass(frozen=True)
class TestCaseDefinition:
    """Contains the parameters necessary to execute a test."""

    executable: pathlib.Path
    args: list[str]
    references: pathlib.Path
    compare_stdout: Callable[[str, str, str], None]
    compare_file_tolerance: float
    ae108_output: list[BinaryOutputDefinition]
    mpi_processes: int


@dataclass(frozen=True)
class TestCaseDefinitions:
    """Representation of a test case definitions json file."""

    executable: pathlib.Path
    args: list[str] = field(default_factory=list)
    compare_stdout: Callable[[str, str, str], None] = no_diff
    compare_file_tolerance: float = 1e-7
    ae108_output: list[BinaryOutputDefinition] = field(default_factory=list)
    mpi_processes: list[int] = field(default_factory=lambda: [1])

    @classmethod
    def from_dict(cls, data: dict[str, Any]) -> "TestCaseDefinitions":
        string_to_comparison_type = {
            "none": no_diff,
            "text": diff_text,
            "numeric": diff_numeric(abs_tol=1e-7, rel_tol=0.0),
        }
        return cls(
            executable=pathlib.Path.cwd() / pathlib.Path(*data.pop("executable")),
            compare_stdout=string_to_comparison_type[
                data.pop("compare_stdout", "none")
            ],
            ae108_output=[
                BinaryOutputDefinition.from_dict(output)
                for output in data.pop("ae108_output", [])
            ],
            **data,
        )

    def to_definitions(self, path: pathlib.Path) -> Iterable[TestCaseDefinition]:
        r"""Generate test case definitions from `definition` for a test at `path`.

        >>> empty_path = pathlib.Path()
        >>> cwd_path = pathlib.Path.cwd()

        >>> next(
        ...     TestCaseDefinitions.from_dict(
        ...         {"executable": ["a", "b"]}
        ...     ).to_definitions(empty_path)
        ... ).executable.relative_to(cwd_path)
        PosixPath('a/b')

        >>> next(
        ...     TestCaseDefinitions.from_dict(
        ...         {"executable": []}
        ...     ).to_definitions(empty_path)
        ... ).args
        []
        >>> next(
        ...     TestCaseDefinitions.from_dict(
        ...         {"executable": [], "args": ["b"]}
        ...     ).to_definitions(empty_path)
        ... ).args
        ['b']

        >>> next(
        ...     TestCaseDefinitions.from_dict(
        ...         {"executable": []}
        ...     ).to_definitions(pathlib.Path("a"))
        ... ).references
        PosixPath('a/references')

        >>> next(
        ...     TestCaseDefinitions.from_dict(
        ...         {"executable": []}
        ...     ).to_definitions(empty_path)
        ... ).compare_stdout is no_diff
        True
        >>> next(
        ...     TestCaseDefinitions.from_dict(
        ...         {"executable": [], "compare_stdout": "text"}
        ...     ).to_definitions(empty_path)
        ... ).compare_stdout is diff_text
        True

        >>> next(
        ...     TestCaseDefinitions.from_dict(
        ...         {"executable": []}
        ...     ).to_definitions(empty_path)
        ... ).mpi_processes
        1
        >>> generator = TestCaseDefinitions.from_dict(
        ...                 {"executable": [], "mpi_processes": [1, 2]}
        ...             ).to_definitions(empty_path)
        >>> list(definitions.mpi_processes for definitions in generator)
        [1, 2]
        """
        for mpi_processes in self.mpi_processes:
            yield (
                TestCaseDefinition(
                    references=path / "references",
                    mpi_processes=mpi_processes,
                    executable=self.executable,
                    args=self.args,
                    compare_stdout=self.compare_stdout,
                    compare_file_tolerance=self.compare_file_tolerance,
                    ae108_output=self.ae108_output,
                )
            )

    def to_test_case(self, path: pathlib.Path) -> type[unittest.TestCase]:
        """Prepare a unittest.TestCase containing test methods for each mpi process value specified in the definitions."""
        group_name, test_name = path.parent.parts[-2:]

        def to_test(definition: TestCaseDefinition) -> staticmethod:
            return staticmethod(lambda: run_testcase(definition))

        return type(
            group_name,
            (unittest.TestCase,),
            {
                f"test_{test_name}_with_{definition.mpi_processes}_mpi_processes": to_test(
                    definition
                )
                for definition in self.to_definitions(path.parent)
            },
        )


def run_executable_with_mpirun(
    executable: pathlib.Path,
    mpi_processes: int,
    args: list[str],
    working_directory: pathlib.Path,
) -> subprocess.CompletedProcess[str]:
    """Run the executable at `executable` with the provided `args`
    from `working_directory` with `mpi_processes` processes.

    >>> empty_path = pathlib.Path()
    >>> run_executable_with_mpirun(
    ...     empty_path,
    ...     mpi_processes = 2,
    ...     args=["-v"],
    ...     working_directory=empty_path
    ... ) # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    subprocess.CalledProcessError: Command '['mpirun', '-n', '2', '.', '-v']' ...
    """
    return run_process(
        args=["mpirun", "-n", str(mpi_processes), str(executable), *args],
        working_directory=working_directory,
    )


def run_process(
    args: list[str],
    working_directory: pathlib.Path | None = None,
) -> subprocess.CompletedProcess[str]:
    """Runs a process with the provided `args` from `working_directory`.

    >>> empty_path = pathlib.Path()
    >>> run_process(
    ...     args=["mpirun", "."],
    ...     working_directory=empty_path
    ... ) # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    subprocess.CalledProcessError: Command '['mpirun', '.']' ...
    """
    if working_directory is None:
        working_directory = pathlib.Path.cwd()
    return subprocess.run(
        args=args,
        cwd=working_directory,
        capture_output=True,
        check=True,
        text=True,
    )


def diff_vtu_files(
    value: pathlib.Path,
    reference: pathlib.Path,
) -> None:
    """Compare the files at `value`, `reference`."""
    if not reference.exists():
        shutil.copy(value, reference)

    run_process(
        [str(ROOT_DIRECTORY / "tests" / "vtu_diff.py"), str(value), str(reference)],
    )


def diff_text(actual: str, expected: str, file_name: str) -> None:
    """Check that the lines in the strings `value` and `reference` are equal.

    >>> diff_text("a", "a", "values.dat")
    >>> diff_text("a", "b", "values.dat")
    Traceback (most recent call last):
    ...
    AssertionError: Test failure:
    --- values.dat
    <BLANKLINE>
    +++ references/values.dat
    <BLANKLINE>
    @@ -1 +1 @@
    <BLANKLINE>
    -a
    +b
    """

    differences = list(
        unified_diff(
            actual.splitlines(),
            expected.splitlines(),
            fromfile=file_name,
            tofile="references/" + file_name,
        )
    )
    if differences:
        msg = "Test failure:\n" + "\n".join(differences[:20])
        raise AssertionError(msg)


def float_or_nan(value: str) -> float:
    """Convert the `value` to float. If this fails then returns NaN.

    >>> float_or_nan("1")
    1.0
    >>> float_or_nan("1.123")
    1.123
    >>> float_or_nan(" 1.123  ")
    1.123
    >>> float_or_nan("ab")
    nan
    >>> float_or_nan(" ")
    nan
    """

    try:
        return float(value)
    except ValueError:
        return math.nan


def extract_numbers(text: Iterable[str]) -> list[list[float]]:
    """Extract the numbers (separated by ',', '(', ')', ']', '[', '=', ';' or whitespace) from text
    and returns a table in for of a nested list, where rows correspond to the
    lines of text and columns to the separated and parsed lines.

    >>> extract_numbers(["1.0, 2.0"])
    [[1.0, 2.0]]
    >>> list(extract_numbers(["x=1.0"]))
    [[nan, 1.0]]
    >>> extract_numbers(["[1.0]"])
    [[1.0]]
    >>> extract_numbers(["1.e1"])
    [[10.0]]
    >>> extract_numbers(["NaN"])
    [[nan]]
    >>> extract_numbers(["1.0 (2.0)"])
    [[1.0, 2.0]]
    >>> extract_numbers(["1.0 a 2.0"])
    [[1.0, nan, 2.0]]
    >>> extract_numbers(["1.0 a 2.0", "b 3.0"])
    [[1.0, nan, 2.0], [nan, 3.0]]
    >>> extract_numbers(["1;2,3", "4,5"])
    [[1.0, 2.0, 3.0], [4.0, 5.0]]
    """
    return [
        [float_or_nan(part) for part in re.split(r"[,;\(\)\[\]=\s]+", line) if part]
        for line in text
    ]


class ExceptionCollector:
    """Context manager collecting errors and raising the collected errors when done."""

    def __init__(self) -> None:
        self.exceptions: list[AssertionError] = []

    def __enter__(self) -> "ExceptionCollector":
        return self

    @contextmanager
    def collect(self):
        """Sub context manager which collects a single exception."""
        try:
            yield
        except AssertionError as e:
            self.exceptions.append(e)

    def __exit__(
        self,
        exc_type: type[BaseException] | None,
        exc_value: BaseException | None,
        traceback: object,
    ):
        if self.exceptions:
            raise AssertionError(
                "Test failure:\n" + "\n".join(format(e) for e in self.exceptions)
            )


def read_file(path: pathlib.Path) -> str:
    """Shortcut for boilerplate code."""
    with open(path, encoding="utf-8") as stream:
        return stream.read()


def diff_numeric(
    abs_tol: float,
    rel_tol: float,
) -> Callable[[str, str, str], None]:
    """Check that the lines in the strings `value` and `reference` are almost equal
    when interpreted as floats. Non-float lines are interpreted as NaNs.

    >>> diff_numeric(0.0, 0.0)("a", "a", "label")
    >>> diff_numeric(0.0, 0.0)("1", "1", "label")
    >>> diff_numeric(0.0, 0.0)("1 2", "1 2", "label")
    >>> diff_numeric(1e-5, 1e-5)("1", "2", "label")
    ... # doctest: +NORMALIZE_WHITESPACE
    Traceback (most recent call last):
        ...
    AssertionError: label: Value mismatch:
    1   ◼: δ=0.5
    >>> diff_numeric(1e-5, 0.0)("a 1", "b 2", "label")
    ... # doctest: +NORMALIZE_WHITESPACE
    Traceback (most recent call last):
        ...
    AssertionError: label: Value mismatch:
    1   ◻◼: δ=0.5
    >>> diff_numeric(0.0, 1e-5)("a", "1", "label")
    ... # doctest: +NORMALIZE_WHITESPACE
    Traceback (most recent call last):
        ...
    AssertionError: label: Value mismatch:
    1   ◼: δ=nan
    >>> diff_numeric(1e-5, 1e-5)("1", "a", "label")
    ... # doctest: +NORMALIZE_WHITESPACE
    Traceback (most recent call last):
        ...
    AssertionError: label: Value mismatch:
    1   ◼: δ=nan
    """

    def diff(value: str, reference: str, file_name: str) -> None:
        try:
            assert_table_close(
                extract_numbers(value.splitlines()),
                extract_numbers(reference.splitlines()),
                abs_tol,
                rel_tol,
            )
        except AssertionError as e:
            raise AssertionError(f"{file_name}: {e}") from None

    return diff


def assert_table_close(
    actual: list[list[float]],
    reference: list[list[float]],
    abs_tol: float,
    rel_tol: float,
) -> None:
    """Assert that tabulated numeric data is close to a reference."""
    if len(actual) != len(reference):
        raise AssertionError(f"Row size mismatch: {len(actual)} != {len(reference)}")
    column_mismatches = [
        (line_no, n_left, n_right)
        for line_no, (n_left, n_right) in enumerate(
            zip(
                (len(row) for row in actual),
                (len(row) for row in reference),
                strict=True,
            ),
            start=1,
        )
        if n_left != n_right
    ]
    if column_mismatches:
        msg = "Column size mismatch:\n" + "\n".join(
            f"{line_no}:\t{left} != {right}"
            for line_no, left, right in column_mismatches
        )
        raise AssertionError(msg)
    deltas = [
        [
            None if is_near(left, right, abs_tol, rel_tol) else rel_delta(left, right)
            for (left, right) in zip(l_row, r_row, strict=True)
        ]
        for l_row, r_row in zip(actual, reference, strict=True)
    ]
    have_mismatches = any(delta is not None for row in deltas for delta in row)
    if have_mismatches:
        rows = (
            (
                i,
                ",".join(f"{delta}" for delta in row if delta is not None),
                "".join("◼" if delta is not None else "◻" for delta in row),
            )
            for i, row in enumerate(deltas, start=1)
            if any(x is not None for x in row)
        )
        msg = "Value mismatch:\n" + "\n".join(
            f"{i}\t{row}: δ={deltas}" for i, deltas, row in rows
        )
        raise AssertionError(msg)


def is_near(actual: float, reference: float, abs_tol: float, rel_tol: float) -> bool:
    """Check if |actual - reference| <= abs_tol + rel_tol * max(|actual|, |reference|)."""
    if math.isnan(actual) != math.isnan(reference):
        return False
    if math.isnan(actual) and math.isnan(reference):
        return True
    return abs(actual - reference) <= abs_tol + rel_tol * max(
        abs(actual), abs(reference)
    )


def rel_delta(actual: float, reference: float):
    """Compute the relative difference of 2 floats."""
    if math.isnan(actual) != math.isnan(reference):
        return float("NAN")
    if max(abs(actual), abs(reference)) == 0.0:
        print(actual, reference)
    return abs(actual - reference) / max(abs(actual), abs(reference))


def load_tests(
    loader: unittest.TestLoader,
    standard_tests: unittest.TestSuite,
    _: Any,
) -> unittest.TestSuite:
    """Use the provided definitions in tests/ to create a TestSuite of tests."""
    paths = (ROOT_DIRECTORY / "tests").glob("*/*/definition.json")

    for path in paths:
        with open(path, encoding="utf-8") as file:
            instance = TestCaseDefinitions.from_dict(json.load(file))
            executable_path = instance.executable
            if not executable_path.exists():
                print(
                    f"Warning: Test '{path.parent}' will be skipped. "
                    f"The executable '{executable_path}' does not exist.",
                    file=sys.stderr,
                )
                continue
            testcase = instance.to_test_case(path)
        standard_tests.addTests(loader.loadTestsFromTestCase(testcase))

    return standard_tests


def run_testcase(definition: TestCaseDefinition) -> None:
    """Run the test defined by `definition` and reports the issues to `test_case`.
    If the environment variable OVERWRITE_REFERENCES is set to 1, reference
    files are created / overwritten.
    """

    overwrite_references = os.environ.get("OVERWRITE_REFERENCES") == "1"
    with tempfile.TemporaryDirectory() as directory, ExceptionCollector() as errors:
        directory_path = pathlib.Path(directory)

        result = run_executable_with_mpirun(
            executable=definition.executable,
            args=definition.args,
            working_directory=directory_path,
            mpi_processes=definition.mpi_processes,
        )

        if overwrite_references:
            with open(
                definition.references / "stdout.txt", "w", encoding="utf-8"
            ) as file:
                file.write(result.stdout)
        with errors.collect():
            definition.compare_stdout(
                result.stdout,
                read_file(definition.references / "stdout.txt"),
                "stdout",
            )

        diff_num = diff_numeric(abs_tol=0.0, rel_tol=definition.compare_file_tolerance)
        diff_by_ext = {
            ".txt": diff_text,
            ".dat": diff_num,
            ".csv": diff_num,
        }

        ae018_exclude = {out.filename.stem + ".vtu" for out in definition.ae108_output}
        for reference in definition.references.iterdir():
            if (
                reference.is_dir()
                or reference.name == "stdout.txt"
                or reference.name in ae018_exclude
            ):
                continue
            if overwrite_references:
                shutil.copy(directory_path / reference.name, reference)
            diff_method = diff_by_ext[reference.suffix]
            with errors.collect():
                diff_method(
                    read_file(directory_path / reference.name),
                    read_file(reference),
                    reference.name,
                )

        for output in definition.ae108_output:
            with tempfile.TemporaryDirectory() as conversion_directory:
                conversion_path = pathlib.Path(conversion_directory)
                run_process(
                    args=[
                        str(ROOT_DIRECTORY / "tools" / "generate_xdmf.py"),
                        *output.xdmf_generator_flags,
                        str(directory_path / output.filename),
                    ],
                    working_directory=conversion_path,
                )
                run_process(
                    [
                        str(ROOT_DIRECTORY / "tools" / "convert_xdmf_to_vtu.py"),
                        str(conversion_path / output.filename.with_suffix(".xdmf")),
                    ],
                    working_directory=conversion_path,
                )
                with errors.collect():
                    diff_vtu_files(
                        conversion_path / output.filename.with_suffix(".vtu"),
                        definition.references / output.filename.with_suffix(".vtu"),
                    )


def main() -> None:
    """Run the tests defined in "definition.json"s."""
    unittest.main()


if __name__ == "__main__":
    main()
